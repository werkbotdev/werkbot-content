<?php
/**/
class ContentLayoutCalendar extends ContentLayout{
  /**/
  public static $singular_name = 'Calendar';
  public static $plural_name = 'Calendars';

  private static $installed = "CalendarPage";
  /**/
  private static $db = array(
    "Content" => "HTMLText",
    "NumberOfEventsToShow" => "Int"
  );
  /**/
  private static $has_one = array(
		'CalendarLink' => 'Sitetree'
  );
  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

    //NUMBER OF EVENTS TO SHOW
    $NumberOfEventsToShow = NumericField::create("NumberOfEventsToShow", "Number of events to show");
    $fields->addFieldToTab("Root.Content", $NumberOfEventsToShow);

    //CALENDAR LINK
    $CalendarLink = TreeDropdownField::create('CalendarLinkID', 'Link to a calendar', 'SiteTree');
    $fields->addFieldToTab("Root.Content", $CalendarLink);

    //CONTENT
		$ContentField = HTMLEditorField::create('Content', 'Content')
      ->setDescription("This will show above the event listing.");
    $fields->addFieldToTab("Root.Content", $ContentField);

    return $fields;

  }
	/**/
	public function onBeforeDelete() {
		$className = get_class($this);
		$id = $this->ID;

		DB::query("DELETE FROM ContentLayoutCalendar_Live WHERE ID=".$id);
		DB::query("DELETE FROM ContentLayoutCalendar_versions WHERE RecordID=".$id);

		parent::onBeforeDelete();
	}
}
