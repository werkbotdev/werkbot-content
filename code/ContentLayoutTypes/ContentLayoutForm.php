<?php
/**/
class ContentLayoutForm extends ContentLayoutHtml{
  /**/
  public static $singular_name = 'Form';
  public static $plural_name = 'Forms';
  /**/
  private static $has_one = array(
    "ContentLayoutUserDefinedForm" => "UserDefinedForm"
  );
  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

    $ContentLayoutUserDefinedForm = DropDownField::create('ContentLayoutUserDefinedFormID', 'Select a Form', UserDefinedForm::get()->map('ID', 'Title'))->setEmptyString('(Select one)');
    $fields->addFieldToTab("Root.Form", $ContentLayoutUserDefinedForm);

    return $fields;

  }
  /**/
  public function getForm(){
    if($this->ContentLayoutUserDefinedFormID){
      $form = UserDefinedForm::get()->byID($this->ContentLayoutUserDefinedFormID);
      if($form){
        $form_controller = new UserDefinedForm_Controller($form);
        return $form_controller;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }
	/**/
	public function onBeforeDelete() {
		$className = get_class($this);
		$id = $this->ID;

		DB::query("DELETE FROM ContentLayoutForm_Live WHERE ID=".$id);
		DB::query("DELETE FROM ContentLayoutForm_versions WHERE RecordID=".$id);

		parent::onBeforeDelete();
	}
}
