<?php
/**/
class ContentLayoutGallery extends ContentLayout{
  /**/
  public static $singular_name = 'Gallery';
  public static $plural_name = 'Galleries';
  /**/
  private static $db = array(
    'Type' => 'Enum("Rotate,Slider", "Rotate")',
    'Controls' => 'Enum("list,arrow", "list")',
    'LinkToOriginal' => 'Enum("Colorbox,Yes,No", "No")',
    'ThumbnailWidth' => "Int",
    'ThumbnailResizeWidth' => "Int",
    'ThumbnailResizeHeight' => "Int",
    'TransitionTime' => "Int"
  );
  /**/
  private static $has_many = array(
    'ContentLayoutGalleryImages' => 'ContentLayoutGalleryImage'
  );
  /**/
  public function getTheTransitionTime(){
    return $this->TransitionTime * 1000;
  }
  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

    //TYPE
    $type = OptionsetField::create(
      'GalleryType1',
      'Type',
      array(
        'Rotate' => 'Simple image rotate',
        'Slider' => 'Thumbnails, click to enlarge'
      ),
      'Rotate'
    );
    $fields->addFieldToTab("Root.GallerySettings", $type);

    //CONTROL
    $control = OptionsetField::create(
      'Controls',
      'Controls',
      array(
        'list' => 'List',
        'arrow' => 'Arrows, next & previous buttons'
      ),
      'list'
    );
    $fields->addFieldToTab("Root.GallerySettings", $control);

    //LINK TO ORIGINAL
    $LinkToOriginal = OptionsetField::create(
      'LinkToOriginal',
      'Link to original image?',
      array(
        'No' => 'No',
        'Yes' => 'Yes, the image will appear in a new window',
        'Colorbox' => 'Yes, the image will appear in modal window',
      ),
      'No'
    );
    $fields->addFieldToTab("Root.GallerySettings", $LinkToOriginal);

    //TRANSITION TIME
    $TransitionTime = NumericField::create("TransitionTime", "Transition Time")
      ->setDescription("In seconds");
    $fields->addFieldToTab("Root.GallerySettings", $TransitionTime);

    //THUMBNAIL WIDTH
    $ThumbnailWidth = NumericField::create("ThumbnailWidth", "Thumbnail Width")
      ->setDescription("Will resize image by width in pixels, width/height ratio remains the same");
    $fields->addFieldToTab("Root.GallerySettings", $ThumbnailWidth);

    //THUMBNAIL RESIZE WIDTH
    $ThumbnailResizeWidth = NumericField::create("ThumbnailResizeWidth", "Thumbnail Resize Width")
      ->setDescription("Crop the image by width");
    $fields->addFieldToTab("Root.GallerySettings", $ThumbnailResizeWidth);

    //THUMBNAIL RESIZE HEIGHT
    $ThumbnailResizeHeight = NumericField::create("ThumbnailResizeHeight", "Thumbnail Resize Height")
      ->setDescription("Crop the image by height");
    $fields->addFieldToTab("Root.GallerySettings", $ThumbnailResizeHeight);

    //GALLERY IMAGES
    $config = GridFieldConfig_RecordEditor::create();
    $config->addComponent(new GridFieldBulkUpload());
    $config->addComponent(new GridFieldBulkManager());
    $config->addComponent(new GridFieldSortableRows('SortOrder'));
    $Gallery = GridField::create(
      'ContentLayoutGalleryImages',
      'Gallery Images',
      $this->ContentLayoutGalleryImages(),
      $config
    );
    $fields->addFieldToTab("Root.GalleryImages", $Gallery);

    return $fields;

  }
	/**/
	public function onBeforeDelete() {
		$className = get_class($this);
		$id = $this->ID;

		DB::query("DELETE FROM ContentLayoutGallery_Live WHERE ID=".$id);
		DB::query("DELETE FROM ContentLayoutGallery_versions WHERE RecordID=".$id);

		parent::onBeforeDelete();
	}
}
