<?php
/**/
class ContentLayoutGalleryImage extends DataObject {
	/**/
	private static $singular_name = 'Gallery Image';
  private static $plural_name = 'Gallery Images';
	/**/
  private static $db = array(
		'Title' => 'Text',
		'Caption' => 'Text',
		'SortOrder' => 'Int'
	);
	/**/
  private static $has_one = array(
		'GalleryImage' => 'Image',
		'ContentLayoutGallery' => 'ContentLayoutGallery'
	);
	/**/
	public static $default_sort = 'SortOrder';
	/**/
	public static $summary_fields = array(
		'Thumbnail' => 'Thumbnail',
		'Title' => 'Title'
 	);
	/**/
	public function getThumbnail() {
		if ($Image = $this->GalleryImage()->ID) {
			return $this->GalleryImage()->SetWidth(80);
		} else {
			return '(No Image)';
		}
	}
	/**/
	public function getCMSFields() {
		//TITLE
		$Title = TextAreaField::create('Title', 'Title')
			->setAttribute('placeholder', 'Please enter the title');

		//CAPTION
		$Caption = TextAreaField::create('Caption', 'Caption')
			->setAttribute('placeholder', 'Please enter a caption');

		//UPLOAD
		$uploadField = UploadField::create('GalleryImage','Image')
			->setFolderName('ContentLayoutGalleryImages');
		$uploadField->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png'));


		return new FieldList(
			$Title,
			$Caption,
			$uploadField
		);

	}
}
