<?php
/**/
class ContentLayoutGroup extends ContentLayout{
  /**/
  public static $singular_name = 'Group';
  public static $plural_name = 'Groups';

  private static $hidden = true;
  private static $literal = true;
  /**/
  private static $db = array(
		"SectionWrapper" => "Enum('no,yes','yes')",
		"ContainerWrapper" => "Enum('no,yes','no')",
    "DesktopFlexDirection" => "Enum('ltr,rtl,ttb,btt','ltr')",
    "TabletFlexDirection" => "Enum('ltr,rtl,ttb,btt','ltr')",
    "PhoneFlexDirection" => "Enum('ltr,rtl,ttb,btt','ltr')"
  );
  /**/
	private static $has_one = array(
		'End' => 'ContentLayoutGroupEnd'
	);
  /**/
  private static $defaults = array(
    "SpacingTop" => "notop",
    "SpacingLeft" => "noleft",
    "SpacingRight" => "noright",
    "SpacingBottom" => "nobottom"
	);
  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

    $fields->addFieldToTab("Root.SpacingLayout", HeaderField::create("", "Layout Options", 3));

    //
    $RenderAs = OptionsetField::create(
      'RenderAs',
      'Render as',
      array(
        'default' => 'Default',
        'tab-container' => 'Tabbed Container',
        'tab-item' => 'Tabbed Item',
      ),
      'default'
    );
    $fields->addFieldToTab("Root.SpacingLayout", $RenderAs);

		//SECTION WRAPPER
    $SectionWrapper = DisplayLogicWrapper::create(
      OptionsetField::create(
        'SectionWrapper',
        'Section Wrapper',
        array(
          'no' => 'No',
          'yes' => 'Yes'
        ),
        'yes'
      )
    )->displayIf("RenderAs")->isEqualTo("default")->end();
    $fields->addFieldToTab("Root.SpacingLayout", $SectionWrapper);

		//CONTAINER WRAPPER
    $ContainerWrapper = DisplayLogicWrapper::create(
      OptionsetField::create(
        'ContainerWrapper',
        'Container Wrapper',
        array(
          'no' => 'No',
          'yes' => 'Yes'
        ),
        'no'
      )
    )->displayIf("SectionWrapper")->isEqualTo("yes")->andIf("RenderAs")->isEqualTo("default")->end();
    $fields->addFieldToTab("Root.SpacingLayout", $ContainerWrapper);

    //DESKTOP FLEX DIRECTION
    $DesktopFlexDirection = OptionsetField::create(
      'DesktopFlexDirection',
      'Direction',
      array(
        'ltr' => 'Left To Right',
        'rtl' => 'Right To Left',
        'ttb' => 'Top To Bottom',
        'btt' => 'Bottom To Top'
      ),
      'ltr'
    );
    $fields->addFieldToTab("Root.SpacingLayout", $DesktopFlexDirection, "ContentLayoutWidthDesktop");

    //TABLET FLEX DIRECTION
    $TabletFlexDirection = OptionsetField::create(
      'TabletFlexDirection',
      'Direction',
      array(
        'ltr' => 'Left To Right',
        'rtl' => 'Right To Left',
        'ttb' => 'Top To Bottom',
        'btt' => 'Bottom To Top'
      ),
      'ltr'
    );
    $fields->addFieldToTab("Root.SpacingLayout", $TabletFlexDirection, "ContentLayoutWidthTablet");

    //PHONE FLEX DIRECTION
    $PhoneFlexDirection = OptionsetField::create(
      'PhoneFlexDirection',
      'Direction',
      array(
        'ltr' => 'Left To Right',
        'rtl' => 'Right To Left',
        'ttb' => 'Top To Bottom',
        'btt' => 'Bottom To Top'
      ),
      'ltr'
    );
    $fields->addFieldToTab("Root.SpacingLayout", $PhoneFlexDirection, "ContentLayoutWidthPhone");

    return $fields;

  }

  /**/
  public function getDirectionClasses(){
    $class_string = "";
    if($this->DesktopFlexDirection){
      $class_string.= " desktop-".$this->DesktopFlexDirection;
    }
    if($this->TabletFlexDirection){
      $class_string.= " tablet-".$this->TabletFlexDirection;
    }
    if($this->PhoneFlexDirection){
      $class_string.= " phone-".$this->PhoneFlexDirection;
    }
    return $class_string;
  }

  /**/
  public function getCMSTitle() {
		$title = $this->getFieldNumber()
			?: $this->Title
			?: 'group';

    return "Group ".$title;

	}

  /*
   GET INLINE CLASS NAME FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineClassnameField($column, $contentClasses) {
  	return new LabelField($column, $this->CMSTitle);
	}
  /**/
	public function onBeforeDelete() {

    $className = get_class($this);
    $id = $this->ID;

    DB::query("DELETE FROM ContentLayoutGroup_Live WHERE ID=".$id);
    DB::query("DELETE FROM ContentLayoutGroup_versions WHERE RecordID=".$id);

		parent::onBeforeDelete();

		// Ensures EndID is lazy-loaded for onAfterDelete
		$this->EndID;
	}
  /**/
	public function onAfterDelete() {
		parent::onAfterDelete();

		// Delete end
		if(($end = $this->End()) && $end->exists()) {
			$end->delete();
		}
	}
  /**/
  public function onAfterDuplicate($original){
    //CREATE A NEW END
    $GroupEnd = ContentLayoutGroupEnd::create();
    $GroupEnd->PageID = $this->PageID;
    $GroupEnd->GroupID = $this->ID;
    $GroupEnd->SortOrder = $original->End()->SortOrder;
    $GroupEnd->write();

    //
    $this->EndID = $GroupEnd->ID;
    $this->write();

    //
    parent::onAfterDuplicate($original);
  }


}
