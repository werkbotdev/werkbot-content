<?php
/**/
class ContentLayoutImage extends ContentLayout{
  /**/
  public static $singular_name = 'Image';
  public static $plural_name = 'Images';
  /**/
  private static $db = array(
    "Caption" => "Text",
    'LinkToImage' => 'Boolean',
    "LinkClass" => "Text",
		'ExternalLink' => 'Text',
    'NewWindow' => 'Boolean'
  );
  /**/
  private static $has_one = array(
    "ContentLayoutImage" => "Image",
		'InternalLink' => 'Sitetree',
		'FileLink' => 'File'
  );
  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

    //IMAGE
    $ContentLayoutImage = UploadField::create('ContentLayoutImage', 'Image')
      ->setFolderName('ContentLayoutImages');
    $fields->addFieldToTab("Root.Image", $ContentLayoutImage);

    //CAPTION
    $Caption = TextAreaField::Create('Caption', 'Caption');
    $fields->addFieldToTab("Root.Image", $Caption);

    //LINK TO IMAGE
    $LinkToImage = OptionsetField::create(
      'LinkToImage',
      'Link to image',
      array(
        '1' => 'Yes',
        '0' => 'No',
      ),
      'false'
    );
    $fields->addFieldToTab("Root.Link", $LinkToImage);

    //EXTERNAL LINK
    $ExternalLink = TextField::create('ExternalLink', 'Link')
      ->setAttribute('placeholder', 'http://www.example.com')
      ->setAttribute('type', 'url')
      ->setAttribute('pattern', 'https?://.+')
      ->displayIf("LinkToImage")->isEqualTo("0")
      ->end();
    $fields->addFieldToTab("Root.Link", $ExternalLink);

    //INTERNAL PAGE LINK
    $InternalLink = DisplayLogicWrapper::create(
        TreeDropdownField::create('InternalLinkID', 'or select a page from this site', 'SiteTree')
      )
      ->displayIf("LinkToImage")->isEqualTo("0")
      ->end();
    $fields->addFieldToTab("Root.Link", $InternalLink);

    //FILE LINK
    $FileLink = DisplayLogicWrapper::create(
      UploadField::create('FileLink', 'or select/upload a file')
        ->setFolderName('ContentLayoutFiles')
    )
      ->displayIf("LinkToImage")->isEqualTo("0")
      ->end();
    $fields->addFieldToTab("Root.Link", $FileLink);

    //CLASS
    $LinkClass = TextField::create('LinkClass', 'Class')
      ->displayIf("LinkToImage")->isEqualTo("1")
      ->end();
    $fields->addFieldToTab("Root.Link", $LinkClass);

    //NEW WINDOW
    $NewWindow = OptionsetField::create(
      'NewWindow',
      'Open link in a new window?',
      array(
        '1' => 'Yes',
        '0' => 'No',
      ),
      'false'
    );
    $fields->addFieldToTab("Root.Link", $NewWindow);

    return $fields;

  }
	/**/
	public function onBeforeDelete() {
		$className = get_class($this);
		$id = $this->ID;

		DB::query("DELETE FROM ContentLayoutImage_Live WHERE ID=".$id);
		DB::query("DELETE FROM ContentLayoutImage_versions WHERE RecordID=".$id);

		parent::onBeforeDelete();
	}
}
