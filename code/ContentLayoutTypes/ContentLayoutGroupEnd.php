<?php
/**/
class ContentLayoutGroupEnd extends ContentLayout{
  /**/
  public static $singular_name = 'Group End';
  public static $plural_name = 'Groups End';

  private static $hidden = true;
  private static $literal = true;
  /**/
	private static $belongs_to = array(
		'Group' => 'ContentLayoutGroup'
	);
  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

		$fields->removeByName(array('Title', 'TitleTag', 'HtmlId', 'HtmlClass', 'AnchorTag', 'Spacing/Layout', 'Background'));

    $fields->addFieldToTab("Root.Main", LiteralField::create("", "<h3>".$this->getCMSTitle()."</h3>"));

    return $fields;

  }

  public function getCMSTitle() {
		$group = $this->Group();

    if($group && $group->exists()){
      return $group->CMSTitle." end";
    }else{
      return "Group end";
    }
	}

	/**/
	public function CheckForSpacing(){
		if($this->Group()->SpacingTop=="notop" && $this->Group()->SpacingLeft=="noleft" && $this->Group()->SpacingRight=="noright" && $this->Group()->SpacingBottom=="nobottom"){
			return false;
		}else{
			return true;
		}
	}

  /*
   GET INLINE CLASS NAME FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineClassnameField($column, $contentClasses) {
    return new LabelField($column, $this->getCMSTitle());
	}
  /*
   GET INLINE TITLE FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineTitleField($column) {
  	return HiddenField::create($column);
	}
  /*
   GET INLINE WIDTH FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineWidthField($column) {
  	return HiddenField::create($column);
  }
  /*
   GET INLINE RENDER FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineRenderField($column) {
  	return HiddenField::create($column);
  }

  public function onAfterWrite() {
		parent::onAfterWrite();

		// If this is not attached to a group, find the first group prior to this
		// with no end attached
		$group = $this->Group();
		if(!($group && $group->exists()) && $this->PageID) {
			$group = ContentLayoutGroup::get()
				->filter(array(
					'PageID' => $this->PageID,
					'SortOrder:LessThanOrEqual' => $this->SortOrder
				))
				->where('"ContentLayoutGroup"."EndID" IS NULL OR "ContentLayoutGroup"."EndID" = 0')
				->sort('"SortOrder" DESC')
				->first();

			// When a group is found, attach it to this end
			if($group) {
				$group->EndID = $this->ID;
				$group->write();
			}
		}
	}
  /**/
	public function duplicate($doWrite = true) {
    return false;
  }
  /**/
	public function onAfterDelete() {
		parent::onAfterDelete();

		// Delete group
		if(($group = $this->Group()) && $group->exists()) {
			$group->delete();
		}
	}

}
