<?php
/**/
class ContentLayoutHtml extends ContentLayout{
  /**/
  public static $singular_name = 'HTML';
  public static $plural_name = 'HTML';
  /**/
  private static $db = array(
    "Content" => "HTMLText",
    "Collapsible" => "Boolean",
    "CollapsibleContent" => "HTMLText",
    "CollapsibleButtonText" => "Text"
  );
  /**/
  private static $defaults = array(
		'CollapsibleButtonText' => 'Read More'
	);
  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

    //COLLAPSIBLE
    $Collapsible = OptionsetField::create(
      'Collapsible',
      'Collapsible?',
      array(
        '1' => 'Yes',
        '0' => 'No',
      ),
      'false'
    )
      ->setDescription('This will let you hide extra content that the user can then choose to view more or not.');
    $fields->addFieldToTab("Root.Content", $Collapsible);

    //COLLAPSIBLE BUTTON TEXT
    $CollapsibleButtonText = TextField::create('CollapsibleButtonText', 'Collapsible Button Text')
      ->displayIf('Collapsible')->isEqualTo('1')
      ->end();
    $fields->addFieldToTab("Root.Content", $CollapsibleButtonText);

		//CONTENT
		$ContentField = HTMLEditorField::create('Content', 'Content');
    $fields->addFieldToTab("Root.Content", $ContentField);

		//COLLAPSIBLE CONTENT
		$CollapsibleContentField = DisplayLogicWrapper::create(
      HTMLEditorField::create('CollapsibleContent', 'This content will be hidden on page load')
    )
      ->displayIf('Collapsible')->isEqualTo('1')
      ->end();
    $fields->addFieldToTab("Root.Content", $CollapsibleContentField);

    return $fields;

  }
	/**/
	public function onBeforeDelete() {
		$className = get_class($this);
		$id = $this->ID;

		DB::query("DELETE FROM ContentLayoutHtml_Live WHERE ID=".$id);
		DB::query("DELETE FROM ContentLayoutHtml_versions WHERE RecordID=".$id);

		parent::onBeforeDelete();
	}
}
