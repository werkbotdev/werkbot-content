<?php
/**/
class ContentLayoutBlog extends ContentLayout{
  /**/
  public static $singular_name = 'Article';
  public static $plural_name = 'Articles';

  private static $installed = "BlogPage";
  /**/
  private static $db = array(
    "Content" => "HTMLText",
    "NumberOfArticlesToShow" => "Int"
  );
  /**/
  private static $has_one = array(
		'BlogLink' => 'Sitetree'
  );
  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

    //NUMBER OF EVENTS TO SHOW
    $NumberOfArticlesToShow = NumericField::create("NumberOfArticlesToShow", "Number of artciles to show");
    $fields->addFieldToTab("Root.Content", $NumberOfArticlesToShow);

    //BLOG LINK
    $BlogLink = TreeDropdownField::create('BlogLinkID', 'Link to a blog', 'SiteTree');
    $fields->addFieldToTab("Root.Content", $BlogLink);

    //CONTENT
		$ContentField = HTMLEditorField::create('Content', 'Content')
      ->setDescription("This will show above the article listing.");
    $fields->addFieldToTab("Root.Content", $ContentField);

    return $fields;

  }
	/**/
	public function onBeforeDelete() {
		$className = get_class($this);
		$id = $this->ID;

		DB::query("DELETE FROM ContentLayoutBlog_Live WHERE ID=".$id);
		DB::query("DELETE FROM ContentLayoutBlog_versions WHERE RecordID=".$id);

		parent::onBeforeDelete();
	}
}
