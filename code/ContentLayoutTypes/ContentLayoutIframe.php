<?php
/**/
class ContentLayoutIframe extends ContentLayout{
  /**/
  public static $singular_name = 'Iframe';
  public static $plural_name = 'Iframes';
  /**/
  private static $db = array(
    'IFrame' => 'HTMLText',
    "Responsive" => "Enum('yes,no','yes')"
  );
  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

    //RESPONSIVE
    $Responsive = OptionsetField::create(
      'Responsive',
      'Responsive',
      array(
        'yes' => 'Yes',
        'no' => 'No'
      ),
      'yes'
    );
    $fields->addFieldToTab("Root.SpacingLayout", $Responsive);

    //IFRAME
    $Iframe = TextAreaField::create('IFrame', 'Enter Iframe Code');
    $fields->addFieldToTab("Root.Iframe", $Iframe);

    return $fields;

  }
	/**/
	public function onBeforeDelete() {
		$className = get_class($this);
		$id = $this->ID;

		DB::query("DELETE FROM ContentLayoutIframe_Live WHERE ID=".$id);
		DB::query("DELETE FROM ContentLayoutIframe_versions WHERE RecordID=".$id);

		parent::onBeforeDelete();
	}
}
