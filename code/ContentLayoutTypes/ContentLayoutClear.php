<?php
/**/
class ContentLayoutClear extends ContentLayout{
  /**/
  public static $singular_name = 'Clear';
  public static $plural_name = 'Clear';

  private static $hidden = true;
  private static $literal = true;

  /**/
  public function getCMSFields() {

    $fields = parent::getCMSFields();

		$fields->removeByName(array('Title', 'HtmlId', 'HtmlClass', 'AnchorTag', 'Spacing/Layout'));

    return $fields;

  }

  public function getFormField() {
		return null;
	}

  /*
   GET INLINE CLASS NAME FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineClassnameField($column, $contentClasses) {
    return new LabelField($column, 'Clear');
	}
  /*
   GET INLINE TITLE FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineTitleField($column) {
  	return HiddenField::create($column);
	}
  /*
   GET INLINE WIDTH FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineWidthField($column) {
  	return HiddenField::create($column);
  }
  /*
   GET INLINE RENDER FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineRenderField($column) {
  	return HiddenField::create($column);
  }

}
