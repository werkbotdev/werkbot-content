<?php
/**/
class ContentLayoutPageExtender extends DataExtension {
	/**/
	private static $db = array(
		'HasForm' => 'Boolean'
	);
	/**/
	private static $has_many = array(
		'ContentLayouts' => 'ContentLayout'
	);
	/**/
	public function __construct() {
		parent::__construct();
	}
	/**/
	public function updateCMSFields(FieldList $fields) {

		Requirements::javascript(CONTENTLAYOUT_URL_BASE . '/js/ContentLayoutFieldEditor.js');

		//MOVE CONTENT TO ITS OWN TAB
		$fields->removeByName('Content');

    //ADD
    $fields->insertBefore(new Tab('Content', 'Content'), 'SEO');
    $fields->insertAfter(new Tab('AdvancedContent', 'Advanced Content'), 'Content');

		//CONTENT
		$htmlField = HTMLEditorField::create('Content', 'Content')
			->addExtraClass('stacked')
			->setDescription('When pasting from word, use the paste from word function or hit "control-shift-v" on your keyboard.');

		//CREATE CONTENT TAB AND ADD HTML EDITOR FOR CONTENT
		$fields->addFieldsToTab('Root.Content', array(
			$htmlField
		));

    //CONTENT LAYOUTS
    $editableColumns = new GridFieldEditableColumns();
		$ContentLayoutClasses = singleton('ContentLayout')->getContentLayoutClasses();
		$editableColumns->setDisplayFields(array(
      'LevelDepth' => function($record, $column, $grid) {
        return HiddenField::create($column);
      },
			'ClassName' => function($record, $column, $grid) use ($ContentLayoutClasses) {
				if($record instanceof ContentLayout) {
					return $record->getInlineClassnameField($column, $ContentLayoutClasses);
				}
			},
			'Title' => function($record, $column, $grid) {
				if($record instanceof ContentLayout) {
					return $record->getInlineTitleField($column);
				}
			},
			'ContentLayoutWidthDesktop' => function($record, $column, $grid) {
				if($record instanceof ContentLayout) {
					return $record->getInlineWidthField($column);
				}
			}
		));

    $config = GridFieldConfig::create()
			->addComponents(
				$editableColumns,
				new GridFieldButtonRow(),
				GridFieldAddClassesButton::create('ContentLayout')
					->setButtonName('Add Content')
					->setButtonClass('ss-ui-action-constructive'),
        GridFieldAddClassesButton::create('ContentLayoutClear')
					->setButtonName('Clear'),
				GridFieldAddClassesButton::create(array('ContentLayoutGroup', 'ContentLayoutGroupEnd'))
					->setButtonName('Add Group'),
				new GridFieldEditButton(),
				new GridFieldDuplicateAction(),
				new GridFieldDeleteAction(),
				new GridFieldToolbarHeader(),
				new GridFieldOrderableRows('SortOrder'),
				new VersionedRevertDODetailsForm()
			);

			//BULK MANAGER
			$gfbm = new GridFieldBulkManager();
			$gfbm->addBulkAction('publish', 'Publish');
			$gfbm->removeBulkAction("bulkEdit");
			$gfbm->removeBulkAction("unLink");
			$config->addComponent($gfbm);

      $ContentLayoutGridfield = GridField::create(
  			'ContentLayouts',
  			'Content Layouts',
  			$this->owner->ContentLayouts(),
  			$config
  		)
        ->addExtraClass('contentlayout-field-editor');
  		$fields->addFieldToTab('Root.AdvancedContent', $ContentLayoutGridfield);
  }
  /**/
	public function onBeforePublish($original) {
    $this->owner->HasForm = false;
    foreach($this->owner->ContentLayouts() as $content) {
      if($content->ClassName == "ContentLayoutForm"){
        $this->owner->HasForm = true;
      }
    }
  }
  /**/
	public function onAfterPublish($original) {
		// Remove fields on the live table which could have been orphaned.
    if($original->ID > 0){
  		$live = Versioned::get_by_stage("ContentLayout", "Live")
  			->filter('PageID', $original->ID);
  		if($live) {
  			foreach($live as $content) {
  				$content->doDeleteFromStage('Live');
  			}
  		}
    }

		foreach($this->owner->ContentLayouts() as $content) {
			//if($content->isChanged()){
				$content->doPublish('Stage', 'Live');
			//}
		}
	}

  /**/
  public function onAfterUnpublish($page) {
    foreach($page->ContentLayouts() as $content) {
      $content->doDeleteFromStage('Live');
    }
  }

  /**/
  public function onAfterDuplicate($newPage) {
		// List of EditableFieldGroups, where the
		// key of the array is the ID of the old end group
		foreach($this->owner->ContentLayouts() as $content) {
			$newContent = $content->duplicate(true);
      if($newContent){
  			$newContent->PageID = $newPage->ID;
  			$newContent->ParentClass = $newPage->ClassName;
  			$newContent->Version = 0;
  			$newContent->write();
        $newContent->invokeWithExtensions('onAfterDuplicate', $newContent);
      }
		}

		return $newPage;
	}

  /**/
  public function getIsModifiedOnStage($isModified) {
   if(!$isModified) {
     foreach($this->owner->ContentLayouts() as $content) {
       if($content->getIsModifiedOnStage()) {
         $isModified = true;
         break;
       }
     }
   }

   return $isModified;
  }

  /**/
  public function onAfterRevertToLive($page) {
    foreach($page->ContentLayouts() as $content) {
      $content->publish('Live', 'Stage', false);
      $content->writeWithoutVersion();
    }
  }

  /* DEPRECIATED */
	public function includeJS(){
  }

}
/**/
class ContentLayoutPageExtender_Controller extends DataExtension {
  /**/
  public function onAfterInit() {
    if($this->owner->HasForm){
      // load the jquery
      $lang = i18n::get_lang_from_locale(i18n::get_locale());
      Requirements::css('userforms/css/UserForm.css');
      Requirements::javascript('userforms/thirdparty/jquery-validate/jquery.validate.min.js');
      Requirements::add_i18n_javascript('userforms/javascript/lang');
      Requirements::javascript('userforms/javascript/UserForm.js');
      Requirements::javascript("userforms/thirdparty/jquery-validate/localization/messages_{$lang}.min.js");
      Requirements::javascript("userforms/thirdparty/jquery-validate/localization/methods_{$lang}.min.js");
    }
  }
}
