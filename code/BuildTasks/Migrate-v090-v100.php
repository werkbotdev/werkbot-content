<?php
/**/
class ContentLayoutModule_Migrate extends BuildTask {
	protected $title = "Content Layout Module - Migrate";
	protected $description = "Migrate data from version 0.9.0 to version 1.0.0";
	protected $enabled = true;

	function run($request) {

    // PAGES
    $pages = Page::get();
    foreach($pages as $page){
      $TabFlag = false;
      $clss = ContentLayoutSection::get()->filter(array(
        "PageID" => $page->ID
      ));
      $i = 0;
      $len = count($clss);
      foreach($clss as $cls){
        if($cls->ClmType=="tab"){
          if(!$TabFlag){
            $TabFlag = true;
            $tab_container_start = ContentLayoutGroup::create();
            $tab_container_start->Title = "Tab Container Start";
            $tab_container_start->PageID = $cls->PageID;
            $tab_container_start->ContentLayoutWidthDesktop = "100";
            $tab_container_start->RenderAs = "tab-container";
            $tab_container_start->write();
    				//echo "Tab Container Start - ".$tab_container_start->ID."<br />";
          }

          $tab_item_start = ContentLayoutGroup::create();
          $tab_item_start->Title =  $cls->Title;
          $tab_item_start->PageID = $cls->PageID;
          $tab_item_start->RenderAs = "tab-item";
          $tab_item_start->write();
        }

        if($cls->SectionLayout=='one'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
        }else if($cls->SectionLayout=='two'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
          $this->packupcontent($cls->Content2Type, $cls, 2);
        }else if($cls->SectionLayout=='three'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
          $this->packupcontent($cls->Content2Type, $cls, 2);
          $this->packupcontent($cls->Content3Type, $cls, 3);
        }else if($cls->SectionLayout=='four'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
          $this->packupcontent($cls->Content2Type, $cls, 2);
          $this->packupcontent($cls->Content3Type, $cls, 3);
          $this->packupcontent($cls->Content4Type, $cls, 4);
        }else if($cls->SectionLayout=='five'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
          $this->packupcontent($cls->Content2Type, $cls, 2);
          $this->packupcontent($cls->Content3Type, $cls, 3);
          $this->packupcontent($cls->Content4Type, $cls, 4);
          $this->packupcontent($cls->Content5Type, $cls, 5);
        }

        if($cls->ClmType=="tab"){
          $tab_item_end = ContentLayoutGroupEnd::create();
          $tab_item_end->Title =  "Tab Item End";
          $tab_item_end->PageID = $cls->PageID;
          $tab_item_end->ContentLayoutWidthDesktop = "100";
					$tab_item_end->GroupID = $tab_item_start->ID;
          $tab_item_end->write();

          $tab_item_start->End = $tab_item_end;
          $tab_item_start->write();
        }

				if(($TabFlag && $cls->ClmType!="tab") || ($TabFlag && $i == ($len - 1))){
          $TabFlag = false;

					$tab_container_end = ContentLayoutGroupEnd::create();
					$tab_container_end->Title = "Tab Container End";
					$tab_container_end->PageID = $cls->PageID;
					$tab_container_end->GroupID = $tab_container_start->ID;
					$tab_container_end->write();

  				//echo "Tab Container End - ".$tab_container_end->ID."<br /><br />";

					$tab_container_start->EndID = $tab_container_end->ID;
					$tab_container_start->write();
        }
        $i++;
      }
    }

    // ARTICLES
    $articles = Article::get();
    foreach($articles as $article){
      $TabFlag = false;
      $clss = ContentLayoutSection::get()->filter(array(
        "ArticleID" => $article->ID
      ));
      $i = 0;
      $len = count($clss);
      foreach($clss as $cls){
        if($cls->ClmType=="tab"){
          if(!$TabFlag){
            $TabFlag = true;
            $tab_container_start = ContentLayoutGroup::create();
            $tab_container_start->Title = "Tab Container Start";
            $tab_container_start->ArticleID = $cls->ArticleID;
            $tab_container_start->ContentLayoutWidthDesktop = "100";
            $tab_container_start->RenderAs = "tab-container";
            $tab_container_start->write();
    				//echo "Tab Container Start - ".$tab_container_start->ID."<br />";
          }

          $tab_item_start = ContentLayoutGroup::create();
          $tab_item_start->Title =  $cls->Title;
          $tab_item_start->ArticleID = $cls->ArticleID;
          $tab_item_start->RenderAs = "tab-item";
          $tab_item_start->write();
        }

        if($cls->SectionLayout=='one'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
        }else if($cls->SectionLayout=='two'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
          $this->packupcontent($cls->Content2Type, $cls, 2);
        }else if($cls->SectionLayout=='three'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
          $this->packupcontent($cls->Content2Type, $cls, 2);
          $this->packupcontent($cls->Content3Type, $cls, 3);
        }else if($cls->SectionLayout=='four'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
          $this->packupcontent($cls->Content2Type, $cls, 2);
          $this->packupcontent($cls->Content3Type, $cls, 3);
          $this->packupcontent($cls->Content4Type, $cls, 4);
        }else if($cls->SectionLayout=='five'){
          $this->packupcontent($cls->Content1Type, $cls, 1);
          $this->packupcontent($cls->Content2Type, $cls, 2);
          $this->packupcontent($cls->Content3Type, $cls, 3);
          $this->packupcontent($cls->Content4Type, $cls, 4);
          $this->packupcontent($cls->Content5Type, $cls, 5);
        }

        if($cls->ClmType=="tab"){
          $tab_item_end = ContentLayoutGroupEnd::create();
          $tab_item_end->Title =  "Tab Item End";
          $tab_item_end->ArticleID = $cls->ArticleID;
          $tab_item_end->ContentLayoutWidthDesktop = "100";
					$tab_item_end->GroupID = $tab_item_start->ID;
          $tab_item_end->write();

          $tab_item_start->End = $tab_item_end;
          $tab_item_start->write();
        }

				if(($TabFlag && $cls->ClmType!="tab") || ($TabFlag && $i == ($len - 1))){
          $TabFlag = false;

					$tab_container_end = ContentLayoutGroupEnd::create();
					$tab_container_end->Title = "Tab Container End";
					$tab_container_end->ArticleID = $cls->ArticleID;
					$tab_container_end->GroupID = $tab_container_start->ID;
					$tab_container_end->write();

  				//echo "Tab Container End - ".$tab_container_end->ID."<br /><br />";

					$tab_container_start->EndID = $tab_container_end->ID;
					$tab_container_start->write();
        }
        $i++;
      }
    }

  }

  /**/
  function packupcontent($type, $data, $pos) {

    //DETERMINE THE TYPE OF CONTENT
    if($type=='empty'){
      return false;
    }else if($type=='html'){
      $tmp_cl = ContentLayoutHtml::create();
    }else if($type=='video'){
      $tmp_cl = ContentLayoutIframe::create();
    }else if($type=='image'){
      $tmp_cl = ContentLayoutImage::create();
    }else if($type=='iframe'){
      $tmp_cl = ContentLayoutIframe::create();
    }else if($type=='gallery'){
      $tmp_cl = ContentLayoutGallery::create();
    }else if($type=='form'){
      $tmp_cl = ContentLayoutForm::create();
    }

    //GENERAL CONTENT LAYOUT SETTINGS
    $tmp_cl->Title = $data->Title;
    $tmp_cl->PageID = $data->PageID;
    $tmp_cl->ArticleID = $data->ArticleID;
    $tmp_cl->ContentLayoutWidthDesktop = $this->determineWidth($data, $pos);
    $tmp_cl->ContentLayoutWidthPhone = "100"; //DEFAULT TO 100
    $tmp_cl->HtmlId = $data->SectionID;
    $tmp_cl->HtmlClass = $data->SectionBackgroundClass;
    $tmp_cl->AnchorTag = $data->SectionAnchorTag;
    //$tmp_cl->SortOrder = $data->SortOrder;

		if($type=='html'){
			if($pos==1){
				$tmp_cl->Content = $data->Content1;
				$tmp_cl->Collapsible = $data->Collapsible;
				$tmp_cl->CollapsibleContent = $data->ContentNotCollapsible1;
				$tmp_cl->CollapsibleButtonText = "More";
			}else if($pos==2){
				$tmp_cl->Content = $data->Content2;
				$tmp_cl->Collapsible = $data->Collapsible2;
				$tmp_cl->CollapsibleContent = $data->ContentNotCollapsible2;
				$tmp_cl->CollapsibleButtonText = "More";
			}else if($pos==3){
				$tmp_cl->Content = $data->Content3;
				$tmp_cl->Collapsible = $data->Collapsible3;
				$tmp_cl->CollapsibleContent = $data->ContentNotCollapsible3;
				$tmp_cl->CollapsibleButtonText = "More";
			}else if($pos==4){
				$tmp_cl->Content = $data->Content4;
				$tmp_cl->Collapsible = $data->Collapsible4;
				$tmp_cl->CollapsibleContent = $data->ContentNotCollapsible4;
				$tmp_cl->CollapsibleButtonText = "More";
			}else if($pos==5){
				$tmp_cl->Content = $data->Content5;
				$tmp_cl->Collapsible = $data->Collapsible5;
				$tmp_cl->CollapsibleContent = $data->ContentNotCollapsible5;
				$tmp_cl->CollapsibleButtonText = "More";
			}
    }else if($type=='video'){
			if($pos==1){
				if($data->Video1Type=="youtube"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//www.youtube.com/embed/'.$data->Video1.'?rel=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
				}else if($data->Video1Type=="vimeo"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//player.vimeo.com/video/'.$data->Video1.'?api=1&autoplay=0" frameborder="0" allowfullscreen></iframe>';
				}
			}else if($pos==2){
				if($data->Video2Type=="youtube"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//www.youtube.com/embed/'.$data->Video2.'?rel=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
				}else if($data->Video2Type=="vimeo"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//player.vimeo.com/video/'.$data->Video2.'?api=1&autoplay=0" frameborder="0" allowfullscreen></iframe>';
				}
			}else if($pos==3){
				if($data->Video3Type=="youtube"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//www.youtube.com/embed/'.$data->Video3.'?rel=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
				}else if($data->Video3Type=="vimeo"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//player.vimeo.com/video/'.$data->Video3.'?api=1&autoplay=0" frameborder="0" allowfullscreen></iframe>';
				}
			}else if($pos==4){
				if($data->Video4Type=="youtube"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//www.youtube.com/embed/'.$data->Video4.'?rel=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
				}else if($data->Video4Type=="vimeo"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//player.vimeo.com/video/'.$data->Video4.'?api=1&autoplay=0" frameborder="0" allowfullscreen></iframe>';
				}
			}else if($pos==5){
				if($data->Video5Type=="youtube"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//www.youtube.com/embed/'.$data->Video5.'?rel=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
				}else if($data->Video5Type=="vimeo"){
					$tmp_cl->IFrame = '<iframe width="560" height="315" src="//player.vimeo.com/video/'.$data->Video5.'?api=1&autoplay=0" frameborder="0" allowfullscreen></iframe>';
				}
			}
    }else if($type=='image'){
			if($pos==1){
				$tmp_cl->Caption = $data->ImageCaption1;
				$tmp_cl->ExternalLink= $data->SectionImageLink1;
				$tmp_cl->NewWindow = $data->SectionImageLinkNewWindow1;
				$tmp_cl->ContentLayoutImageID = $data->SectionImage1ID;
			}else if($pos==2){
				$tmp_cl->Caption = $data->ImageCaption2;
				$tmp_cl->ExternalLink= $data->SectionImageLink2;
				$tmp_cl->NewWindow = $data->SectionImageLinkNewWindow2;
				$tmp_cl->ContentLayoutImageID = $data->SectionImage2ID;
			}else if($pos==3){
				$tmp_cl->Caption = $data->ImageCaption3;
				$tmp_cl->ExternalLink= $data->SectionImageLink3;
				$tmp_cl->NewWindow = $data->SectionImageLinkNewWindow3;
				$tmp_cl->ContentLayoutImageID = $data->SectionImage3ID;
			}else if($pos==4){
				$tmp_cl->Caption = $data->ImageCaption4;
				$tmp_cl->ExternalLink= $data->SectionImageLink4;
				$tmp_cl->NewWindow = $data->SectionImageLinkNewWindow4;
				$tmp_cl->ContentLayoutImageID = $data->SectionImage4ID;
			}else if($pos==5){
				$tmp_cl->Caption = $data->ImageCaption5;
				$tmp_cl->ExternalLink= $data->SectionImageLink5;
				$tmp_cl->NewWindow = $data->SectionImageLinkNewWindow5;
				$tmp_cl->ContentLayoutImageID = $data->SectionImage5ID;
			}
    }else if($type=='iframe'){
			if($pos==1){
				$tmp_cl->IFrame = $data->IFrame1;
			}else if($pos==2){
				$tmp_cl->IFrame = $data->IFrame2;
			}else if($pos==3){
				$tmp_cl->IFrame = $data->IFrame3;
			}else if($pos==4){
				$tmp_cl->IFrame = $data->IFrame4;
			}else if($pos==5){
				$tmp_cl->IFrame = $data->IFrame5;
			}
    }else if($type=='gallery'){
			if($pos==1){
				$tmp_cl->Type = $data->GalleryType1;
				foreach($data->CLSGalleryImages1() as $tmpimage){
          $tmp = ContentLayoutGalleryImage::create();
          $tmp->Title = $tmpimage->Title;
          $tmp->Caption = $tmpimage->Caption;
          $tmp->SortOrder = $tmpimage->SortOrder;
          $tmp->GalleryImageID = $tmpimage->GalleryImageID;
          $tmp->write();
					$tmp_cl->ContentLayoutGalleryImages()->add($tmp);
				}
			}else if($pos==2){
				$tmp_cl->Type = $data->GalleryType2;
				foreach($data->CLSGalleryImages2() as $tmpimage){
          $tmp = ContentLayoutGalleryImage::create();
          $tmp->Title = $tmpimage->Title;
          $tmp->Caption = $tmpimage->Caption;
          $tmp->SortOrder = $tmpimage->SortOrder;
          $tmp->GalleryImageID = $tmpimage->GalleryImageID;
          $tmp->write();
					$tmp_cl->ContentLayoutGalleryImages()->add($tmp);
				}
			}else if($pos==3){
				$tmp_cl->Type = $data->GalleryType3;
				foreach($data->CLSGalleryImages3() as $tmpimage){
          $tmp = ContentLayoutGalleryImage::create();
          $tmp->Title = $tmpimage->Title;
          $tmp->Caption = $tmpimage->Caption;
          $tmp->SortOrder = $tmpimage->SortOrder;
          $tmp->GalleryImageID = $tmpimage->GalleryImageID;
          $tmp->write();
					$tmp_cl->ContentLayoutGalleryImages()->add($tmp);
				}
			}else if($pos==4){
				$tmp_cl->Type = $data->GalleryType4;
				foreach($data->CLSGalleryImages4() as $tmpimage){
          $tmp = ContentLayoutGalleryImage::create();
          $tmp->Title = $tmpimage->Title;
          $tmp->Caption = $tmpimage->Caption;
          $tmp->SortOrder = $tmpimage->SortOrder;
          $tmp->GalleryImageID = $tmpimage->GalleryImageID;
          $tmp->write();
					$tmp_cl->ContentLayoutGalleryImages()->add($tmp);
				}
			}else if($pos==5){
				$tmp_cl->Type = $data->GalleryType5;
				foreach($data->CLSGalleryImages5() as $tmpimage){
          $tmp = ContentLayoutGalleryImage::create();
          $tmp->Title = $tmpimage->Title;
          $tmp->Caption = $tmpimage->Caption;
          $tmp->SortOrder = $tmpimage->SortOrder;
          $tmp->GalleryImageID = $tmpimage->GalleryImageID;
          $tmp->write();
					$tmp_cl->ContentLayoutGalleryImages()->add($tmp);
				}
			}
    }else if($type=='form'){
			if($pos==1){
				$tmp_cl->Content = $data->FormContent1;
				$tmp_cl->ContentLayoutUserDefinedFormID = $data->UserDefinedForm1ID;
			}else if($pos==2){
				$tmp_cl->Content = $data->FormContent2;
				$tmp_cl->ContentLayoutUserDefinedFormID = $data->UserDefinedForm2ID;
			}else if($pos==3){
				$tmp_cl->Content = $data->FormContent3;
				$tmp_cl->ContentLayoutUserDefinedFormID = $data->UserDefinedForm3ID;
			}else if($pos==4){
				$tmp_cl->Content = $data->FormContent4;
				$tmp_cl->ContentLayoutUserDefinedFormID = $data->UserDefinedForm4ID;
			}else if($pos==5){
				$tmp_cl->Content = $data->FormContent5;
				$tmp_cl->ContentLayoutUserDefinedFormID = $data->UserDefinedForm5ID;
			}

    }

    //WRITE OUT THE CONTENT
    $tmp_cl->write();

  }

  /**/
  function determineWidth($data, $Position){

    if($data->SectionLayout=='one'){
      return "100";
    }else if($data->SectionLayout=='two'){
      //'2575,3070,4060,5050,6040,6634,7030,7525'
      switch($data->TwoColumnLayout){

        case "2535":
          if($Position==1){
            return "25";
          }else if($Position==2){
            return "35";
          }
        break;
        case "3070":
          if($Position==1){
            return "30";
          }else if($Position==2){
            return "70";
          }
        break;
        case "4060":
          if($Position==1){
            return "40";
          }else if($Position==2){
            return "60";
          }
        break;
        case "5050":
          if($Position==1){
            return "50";
          }else if($Position==2){
            return "50";
          }
        break;
        case "6040":
          if($Position==1){
            return "60";
          }else if($Position==2){
            return "40";
          }
        break;
        case "6634":
          if($Position==1){
            return "66";
          }else if($Position==2){
            return "34";
          }
        break;
        case "7030":
          if($Position==1){
            return "70";
          }else if($Position==2){
            return "30";
          }
        break;
        case "7525":
          if($Position==1){
            return "75";
          }else if($Position==2){
            return "25";
          }
        break;

        default:
          return "100";
      }
    }else if($data->SectionLayout=='three'){
      //'333333,252550,1004060,505050,604040,703030,505050-flipped'
      switch($data->ThreeColumnLayout){

        case "333333":
          return "33";
        break;
        case "252550":
          if($Position==1){
            return "25";
          }else if($Position==2){
            return "25";
          }else if($Position==3){
            return "50";
          }
        break;
        case "1004060":
          if($Position==1){
            return "100";
          }else if($Position==2){
            return "40";
          }else if($Position==3){
            return "60";
          }
        break;

        case "505050":
          return "50";
        break;

        case "604040":
          if($Position==1){
            return "60";
          }else if($Position==2){
            return "40";
          }else if($Position==3){
            return "40";
          }
        break;

        case "703030":
          if($Position==1){
            return "70";
          }else if($Position==2){
            return "30";
          }else if($Position==3){
            return "30";
          }
        break;

        case "505050-flipped":
          return "50";
        break;


        default:
          return "100";
      }

    }else if($data->SectionLayout=='four'){
      //'25252525,1005050100,25755050,50505050'
      switch($data->ThreeColumnLayout){
        case "25252525":
          return "25";
        break;
        case "1005050100":
          if($Position==1){
            return "100";
          }else if($Position==2){
            return "50";
          }else if($Position==3){
            return "50";
          }else if($Position==4){
            return "100";
          }
        break;
        case "25755050":
          if($Position==1){
            return "25";
          }else if($Position==2){
            return "75";
          }else if($Position==3){
            return "50";
          }else if($Position==4){
            return "50";
          }
        break;
        case "50505050":
          return "50";
        break;
        default:
          return "100";
      }
    }else if($data->SectionLayout=='five'){
      return "20";
    }

  }

}
