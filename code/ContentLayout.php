<?php
/*
  CONTENT LAYOUT
  All content types must extened this obejct.
  The default use for this is for layout purposes.
*/
class ContentLayout extends DataObject{
	/**/
	public static $singular_name = 'FPO';
	public static $plural_name = 'FPO';

	private static $hidden = false;
  private static $abstract = true;
  private static $literal = false;
  private static $installed = "";

  /**/
  private static $summary_fields = array(
	  'Title'
  );
  /**/
  private static $db = array(
    "Title" => "Text",
    "Width" => "Text",
    "HtmlId" => "Text",
    "HtmlClass" => "Text",
    "AnchorTag" => "Text",
    "TitleTag" => "Text",

    "SpacingTop" => "Enum('default,halftop,notop','default')",
    "SpacingLeft" => "Enum('default,halfleft,noleft','default')",
    "SpacingRight" => "Enum('default,halfright,noright','default')",
    "SpacingBottom" => "Enum('default,halfbottom,nobottom','default')",

		"RenderAs" => "Enum('default,tab-item,tab-container','default')",

    "BackgroundColor" => "Color",
    "BackgroundStyle" => "Text",

    'SortOrder' => 'Int',

    'ContentLayoutWidthDesktop' => 'Int',
    'ContentLayoutWidthTablet' => 'Int',
    'ContentLayoutWidthPhone' => 'Int'
  );
  /**/
  private static $has_one = array(
    'Page' => 'Page',
		'BackgroundImage' => 'Image'
  );
  /**/
  public static $default_sort = 'SortOrder ASC';
	/**/
	private static $extensions = [
		'Heyday\VersionedDataObjects\VersionedDataObject'
	];
  /*
    GET CMS FIELDS
  */
  public function getCMSFields() {

    //HEADER FIELD
    $Header = HeaderField::create("", $this->i18n_singular_name() , 2);

    //TITLE
    $Title = TextField::create('Title', 'Title')
      ->setAttribute('placeholder', 'Please enter the title of this piece of content');

    //HTML ID
    $HtmlId = TextField::create('HtmlId', 'ID');

    //HTML CLASS
    $HtmlClass = TextField::create('HtmlClass', 'Class')
			->setDescription("To add multiple classes just separate the class names by a space.");

    //ANCHOR TAG
    $AnchorTag = TextField::create('AnchorTag', 'Anchor Tag');

		//TITLE TAG
		$TitleTag = TextField::create('TitleTag', 'Title Tag');

    //WIDTHS
    $widths = array();
    for($i=100;$i>0;$i--){
      $widths[$i] = $i."%";
    }
    //$ContentLayoutWidthDefault = DropdownField::create('Width', 'Default', $widths, $this->Width);
    $ContentLayoutWidthDesktop = DropdownField::create('ContentLayoutWidthDesktop', 'Width', $widths)
			->setEmptyString("Select a width");
    $ContentLayoutWidthTablet = DropdownField::create('ContentLayoutWidthTablet', 'Width', $widths)
			->setEmptyString("Select a width");
    $ContentLayoutWidthPhone = DropdownField::create('ContentLayoutWidthPhone', 'Width', $widths)
			->setEmptyString("Select a width");

    //SPACING - TOP
    $SpacingTop = OptionsetField::create(
      'SpacingTop',
      'Top',
      array(
        'default' => 'Default',
        'halftop' => 'Half',
        'notop' => 'None'
      ),
      'default'
    );
    //SPACING - LEFT
    $SpacingLeft = OptionsetField::create(
      'SpacingLeft',
      'Left',
      array(
        'default' => 'Default',
        'halfleft' => 'Half',
        'noleft' => 'None'
      ),
      'default'
    );
    //SPACING - RIGHT
    $SpacingRight = OptionsetField::create(
      'SpacingRight',
      'Right',
      array(
        'default' => 'Default',
        'halfright' => 'Half',
        'noright' => 'None'
      ),
      'default'
    );
    //SPACING - BOTTOM
    $SpacingBottom = OptionsetField::create(
      'SpacingBottom',
      'Bottom',
      array(
        'default' => 'Default',
        'halfbottom' => 'Half',
        'nobottom' => 'None'
      ),
      'default'
    );
		//BACKGROUND
    $BackgroundImage = UploadField::create('BackgroundImage', 'Image')
      ->setFolderName('BackgroundImages');

		//BACKGROUND COLOR
		$BackgroundColor = ColorField::create('BackgroundColor', 'Background color');

		//BACKGROUND STYLE OPTIONS
		$BackgroundStyle = TextField::create('BackgroundStyle', 'Background style');

    $fields = new FieldList(
      new TabSet(
        'Root',
        new Tab(
          'Main',
					$Header,
          $Title,
          $HtmlId,
          $HtmlClass,
					$AnchorTag,
					$TitleTag
        ),
        new Tab(
          'Spacing/Layout',
					HeaderField::create("", "Breakpoint Options", 3),
					HeaderField::create("", "Desktop", 4),
          $ContentLayoutWidthDesktop,
					HeaderField::create("", "Tablet", 4),
          $ContentLayoutWidthTablet,
					HeaderField::create("", "Phone", 4),
          $ContentLayoutWidthPhone,
					HeaderField::create("", "Spacing Options", 3),
          $SpacingTop,
          $SpacingLeft,
          $SpacingRight,
          $SpacingBottom
        ),
        new Tab(
          'Background',
					HeaderField::create("", "Background", 3),
          $BackgroundImage,
					$BackgroundColor,
					$BackgroundStyle
        )
      )
    );

    return $fields;
  }
  /**/
  public function getBreakPointClasses(){
    $class_string = "";
    if($this->ContentLayoutWidthDesktop){
      $class_string.= " desktop-".$this->ContentLayoutWidthDesktop;
    }
    if($this->ContentLayoutWidthTablet){
      $class_string.= " tablet-".$this->ContentLayoutWidthTablet;
    }
    if($this->ContentLayoutWidthPhone){
      $class_string.= " phone-".$this->ContentLayoutWidthPhone;
    }
    return $class_string;
  }
	/**/
	public function getCMSActions(){
		$actions = parent::getCMSActions();
		$Action = new FormAction(
		   "duplicate",
		   "Duplicate Content Layout"
		);
		$actions->push($Action);

		return $actions;
	}
  /**/
  public function Link(){
	   return $this->Page()->Link();
  }
  /**/
  public function AbsoluteLink(){
	   return $this->Page()->AbsoluteLink();
  }
	/**/
	public function onBeforeWrite() {
		parent::onBeforeWrite();

		if(!$this->SortOrder && $this->PageID) {
			$pageID = $this->PageID;
			$this->SortOrder = ContentLayout::get()
				->filter('PageID', $pageID)
				->max('SortOrder') + 1;
		}
	}

	/**/
	public function CheckForSpacing(){
		if($this->SpacingTop=="notop" && $this->SpacingLeft=="noleft" && $this->SpacingRight=="noright" && $this->SpacingBottom=="nobottom"){
			return false;
		}else{
			return true;
		}
	}

  /*
    GET INCLUDE FILE
  */
  public function getIncludeFile(){

    return $this->renderWith($this->ClassName, "ContentLayout");
  }
  /*
   GET INLINE CLASS NAME FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineClassnameField($column, $contentClasses) {
		return DropdownField::create($column, false, $contentClasses)
			->setEmptyString("Select a type");
	}
  /*
   GET INLINE TITLE FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineTitleField($column) {
		return TextField::create($column, false)
			->setAttribute('placeholder', 'Title')
			->setAttribute('data-placeholder', 'Title');
	}
  /*
   GET INLINE WIDTH FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineWidthField($column) {
		$widths = array();
		for($i=100;$i>0;$i--){
			$widths[$i] = $i."%";
		}
		return DropdownField::create($column, false, $widths, "100")
			->setEmptyString("Select a width");
	}
  /*
   GET INLINE RENDER FIELD
   Called by gridfield (ContentLayoutPageExtender.php)
  */
	public function getInlineRenderField($column) {
		return DropdownField::create($column, false, array(
			'default' => 'Default',
			'tab-container' => 'Tab Container',
			'tab-item' => 'Tab Item'
		));
	}
	/**
	 * Find the numeric indicator (1.1.2) that represents it's nesting value
	 *
	 * Only useful for fields attached to a current page, and that contain other fields such as pages
	 * or groups
	 *
	 * @return string
	 */
	public function getFieldNumber() {
		// Check if exists
		if(!$this->exists()) {
			return null;
		}
		// Check parent
		$page = $this->Page();
		if(!$page || !$page->exists() || !($contentlayouts = $page->ContentLayouts())) {
			return null;
		}

		$prior = 0; // Number of prior group at this level
		$stack = array(); // Current stack of nested groups, where the top level = the page
		foreach($contentlayouts->map('ID', 'ClassName') as $id => $className) {
			if($className === 'ContentLayoutGroup') {
				$stack[] = $prior + 1;
				$prior = 0;
			} elseif($className === 'ContentLayoutGroupEnd') {
				$prior = array_pop($stack);
			}
			if($id == $this->ID) {
				return implode('.', $stack);
			}
		}
		return null;
	}
  /*
    GET CONTENT LAYOUT CLASSES
  */
	public function getContentLayoutClasses($includeLiterals = true) {
		$classes = ClassInfo::getValidSubClasses('ContentLayout');

		// Remove classes we don't want to display in the dropdown.
		$ContentLayoutClasses = array();
		foreach ($classes as $class) {
      // Check For Existence
			if(Config::inst()->get($class, 'installed')!="") {
			  if (!class_exists(Config::inst()->get($class, 'installed'))) {
  				continue;
        }
			}

			// Skip abstract / hidden classes
			if(Config::inst()->get($class, 'abstract', Config::UNINHERITED) || Config::inst()->get($class, 'hidden')) {
				continue;
			}

			if(!$includeLiterals && Config::inst()->get($class, 'literal')) {
				continue;
			}

			$singleton = singleton($class);
			if(!$singleton->canCreate()) {
				continue;
			}

			$ContentLayoutClasses[$class] = $singleton->i18n_singular_name();
		}

		asort($ContentLayoutClasses);
		return $ContentLayoutClasses;
	}
  /*
    IS NEW
	  checks wether record is new, copied from Sitetree
  */
	function isNew() {
		if(empty($this->ID)) return true;

		if(is_numeric($this->ID)) return false;

		return stripos($this->ID, 'new') === 0;
	}
  /*
    GET IS MODIFIED ON STAGE
	  checks if records is changed on stage
  */
	public function getIsModifiedOnStage() {
		// new unsaved fields could be never be published
		if($this->isNew()) return false;

		$stageVersion = Versioned::get_versionnumber_by_stage('ContentLayout', 'Stage', $this->ID);
		$liveVersion = Versioned::get_versionnumber_by_stage('ContentLayout', 'Live', $this->ID);

		return ($stageVersion && $stageVersion != $liveVersion);
	}
  /*
    DO DELETE FROM STAGE
    Delete this field from a given stage
  */
	public function doDeleteFromStage($stage) {
		// Remove record
		$this->deleteFromStage($stage);
	}
  /*
    DO PUBLISH
    Publish this Form Field to the live site
  */
	public function doPublish($fromStage, $toStage, $createNewVersion = false) {
		$this->publish($fromStage, $toStage, $createNewVersion);
	}
  /**/
  public function GridFieldRowClasses() {
		$classes = array();

    if($this->getIsModifiedOnStage()){
      $classes[] = "status-draft";
    }else{
      $classes[] = "status-live";
    }

		return $classes;
	}
  /**/
	public function duplicate($doWrite = true) {
		$className = $this->class;
		$clone = new $className( $this->toMap(), false, $this->model );
		$clone->ID = 0;

		if($doWrite) {
			$clone->write();
		  $this->duplicateManyManyRelations($this, $clone);
		}

		return $clone;
	}
	/**/
	public function onBeforeDelete() {
		$className = get_class($this);
		$id = $this->ID;

		DB::query("DELETE FROM ContentLayout_Live WHERE ID=".$id);
		DB::query("DELETE FROM ContentLayout_versions WHERE RecordID=".$id);

		parent::onBeforeDelete();
	}
  /**/
  public function onAfterDuplicate($original){
  }
}
