<?php

/**/
class ContentLayoutSection extends DataObject
{
    /**/
    public static $singular_name = 'Section';
    public static $plural_name = 'Sections';
    /**/
    private static $db = array(
        'Title' => 'Text',
        'ClmType' => "Enum('default,tab', 'default')",
        'TitleHTag' => "Enum('h1,h2,h3,h4,h5,h6', 'h2')",
        'ShowHideTitle' => "Enum('hide,show', 'hide')",
        'SectionImageMinHeight' => 'Text',
        'SectionBackgroundClass' => 'Text',
        'SectionBackgroundColor' => 'Text',
        'SectionID' => 'Text',
        'SectionAnchorTag' => 'Text',
        'Content1' => 'HTMLText',
        'Content2' => 'HTMLText',
        'Content3' => 'HTMLText',
        'Content4' => 'HTMLText',
        'Content5' => 'HTMLText',
        'ContentNotCollapsible1' => 'HTMLText',
        'ContentNotCollapsible2' => 'HTMLText',
        'ContentNotCollapsible3' => 'HTMLText',
        'ContentNotCollapsible4' => 'HTMLText',
        'ContentNotCollapsible5' => 'HTMLText',
        'Video1' => 'Text',
        'Video1Type' => "Enum('youtube,vimeo,none', 'none')",
        'Video2' => 'Text',
        'Video2Type' => "Enum('youtube,vimeo,none', 'none')",
        'Video3' => 'Text',
        'Video3Type' => "Enum('youtube,vimeo,none', 'none')",
        'Video4' => 'Text',
        'Video4Type' => "Enum('youtube,vimeo,none', 'none')",
        'Video5' => 'Text',
        'Video5Type' => "Enum('youtube,vimeo', 'youtube')",
        'Content1Type' => "Enum('empty,html,video,image,iframe,gallery,blog,calendar,form,testimonial', 'empty')",
        'Content2Type' => "Enum('empty,html,video,image,iframe,gallery,blog,calendar,form,testimonial', 'empty')",
        'Content3Type' => "Enum('empty,html,video,image,iframe,gallery,blog,calendar,form,testimonial', 'empty')",
        'Content4Type' => "Enum('empty,html,video,image,iframe,gallery,blog,calendar,form,testimonial', 'empty')",
        'Content5Type' => "Enum('empty,html,video,image,iframe,gallery,blog,calendar,form,testimonial', 'empty')",
        'SectionLayout' => "Enum('one,two,three,four,five', 'one')",
        'TwoColumnLayout' => "Enum('2575,3070,4060,5050,6040,6634,7030,7525', '5050')",
        'TwoColumnImageLayout' => "Enum('left,right', 'left')",
        'TwoColumnHangImageLayout' => "Enum('yes,no', 'no')",
        'TwoColumnHangImageLayoutMaxWidth' => 'Int',
        'ThreeColumnLayout' => "Enum('333333,252550,1004060,505050,604040,703030,505050-flipped', '333333')",
        'FourColumnLayout' => "Enum('25252525,1005050100,25755050,50505050', '25252525')",
        'IFrame1' => 'HTMLText',
        'IFrame2' => 'HTMLText',
        'IFrame3' => 'HTMLText',
        'IFrame4' => 'HTMLText',
        'IFrame5' => 'HTMLText',
        'ContainContent' => 'Boolean',
        'Collapsible' => 'Boolean',
        'Collapsible2' => 'Boolean',
        'Collapsible3' => 'Boolean',
        'Collapsible4' => 'Boolean',
        'Collapsible5' => 'Boolean',
        'GalleryTitle1' => 'Text',
        'GalleryTitle2' => 'Text',
        'GalleryTitle3' => 'Text',
        'GalleryTitle4' => 'Text',
        'GalleryTitle5' => 'Text',
        'GalleryType1' => "Enum('Rotate,Slider', 'Rotate')",
        'GalleryType2' => "Enum('Rotate,Slider', 'Rotate')",
        'GalleryType3' => "Enum('Rotate,Slider', 'Rotate')",
        'GalleryType4' => "Enum('Rotate,Slider', 'Rotate')",
        'GalleryType5' => "Enum('Rotate,Slider', 'Rotate')",
        'GalleryThumbnailSize1' => 'Text',
        'GalleryThumbnailSize2' => 'Text',
        'GalleryThumbnailSize3' => 'Text',
        'GalleryThumbnailSize4' => 'Text',
        'GalleryThumbnailSize5' => 'Text',
        'BlogNumberOfArticles1' => 'Int',
        'BlogNumberOfArticles2' => 'Int',
        'BlogNumberOfArticles3' => 'Int',
        'BlogNumberOfArticles4' => 'Int',
        'BlogNumberOfArticles5' => 'Int',
        'CalendarNumberOfEvents1' => 'Int',
        'CalendarNumberOfEvents2' => 'Int',
        'CalendarNumberOfEvents3' => 'Int',
        'CalendarNumberOfEvents4' => 'Int',
        'CalendarNumberOfEvents5' => 'Int',
        'ImageCaption1' => 'Text',
        'ImageCaption2' => 'Text',
        'ImageCaption3' => 'Text',
        'ImageCaption4' => 'Text',
        'ImageCaption5' => 'Text',
        'FormContent1' => 'HTMLText',
        'FormContent2' => 'HTMLText',
        'FormContent3' => 'HTMLText',
        'FormContent4' => 'HTMLText',
        'FormContent5' => 'HTMLText',
        'ResponsiveContent1' => 'Boolean',
        'ResponsiveContent2' => 'Boolean',
        'ResponsiveContent3' => 'Boolean',
        'ResponsiveContent4' => 'Boolean',
        'ResponsiveContent5' => 'Boolean',

        'SectionImageLink1' => 'Text',
        'SectionImageLink2' => 'Text',
        'SectionImageLink3' => 'Text',
        'SectionImageLink4' => 'Text',
        'SectionImageLink5' => 'Text',
        'SectionImageLinkNewWindow1' => 'Boolean',
        'SectionImageLinkNewWindow2' => 'Boolean',
        'SectionImageLinkNewWindow3' => 'Boolean',
        'SectionImageLinkNewWindow4' => 'Boolean',
        'SectionImageLinkNewWindow5' => 'Boolean',

        'SortOrder' => 'Int',
    );
    /**/
    private static $has_one = array(
        'Page' => 'Page',
        'Article' => 'Article',
        'SectionBackgroundImage' => 'Image',

        'SectionImage1' => 'Image',
        'SectionImage2' => 'Image',
        'SectionImage3' => 'Image',
        'SectionImage4' => 'Image',
        'SectionImage5' => 'Image',

        'Video1Image' => 'Image',
        'Video2Image' => 'Image',
        'Video3Image' => 'Image',
        'Video4Image' => 'Image',
        'Video5Image' => 'Image',

        'UserDefinedForm1' => 'UserDefinedForm',
        'UserDefinedForm2' => 'UserDefinedForm',
        'UserDefinedForm3' => 'UserDefinedForm',
        'UserDefinedForm4' => 'UserDefinedForm',
        'UserDefinedForm5' => 'UserDefinedForm',
    );
    /**/
    private static $has_many = array(
        'CLSGalleryImages1' => 'CLSGalleryImage',
        'CLSGalleryImages2' => 'CLSGalleryImage',
        'CLSGalleryImages3' => 'CLSGalleryImage',
        'CLSGalleryImages4' => 'CLSGalleryImage',
        'CLSGalleryImages5' => 'CLSGalleryImage',
    );
    /**/
    private static $defaults = array(
        'ContainContent' => true,
        'ResponsiveContent1' => true,
        'ResponsiveContent2' => true,
        'ResponsiveContent3' => true,
        'ResponsiveContent4' => true,
        'ResponsiveContent5' => true,
        'GalleryThumbnailSize1' => 100,
        'GalleryThumbnailSize2' => 100,
        'GalleryThumbnailSize3' => 100,
        'GalleryThumbnailSize4' => 100,
        'GalleryThumbnailSize5' => 100
    );
    /**/
    public static $default_sort = 'SortOrder ASC';
    /**/
    private static $summary_fields = array(
        'SortOrder' => '#',
        'ClmType' => 'Type',
        'Status' => 'Status',
        'Title' => 'Title',
    );
    /**/
    private static $create_table_options = array(
        'MySQLDatabase' => 'ENGINE=MyISAM',
    );
    /**/
    public function canCreate($member = null)
    {
        return true;
    }
    /**/
    public function canView($member = null)
    {
        return true;
    }
    /**/
    public function canEdit($member = null)
    {
        return true;
    }
    /**/
    public function canDelete($member = null)
    {
        return true;
    }
    /**/
    public function getCMSValidator()
    {
        return new ContentLayoutSection_Validator();
    }
    /**/
    public function getStatus()
    {
        if ($this->isPublished()) {
            return 'Live';
        } else {
            return 'Draft';
        }
    }
    /**/
    public function getCMSFields()
    {
        //TITLE
        $Title = TextAreaField::create('Title', 'Title')
            ->setAttribute('placeholder', 'Please enter the title of the section');
        //TYPE
        $ClmType = DropdownField::create('ClmType', 'Type', singleton('ContentLayoutSection')->dbObject('ClmType')->enumValues(), 'default')
            ->setDescription('When you create multiple "tab" sections, they will form a group of tabs in the content of the page.');
        //TITLE HEADING TAG
        $TitleHTag = DropdownField::create('TitleHTag', 'Heading Tag', singleton('ContentLayoutSection')->dbObject('TitleHTag')->enumValues(), 'h2');
        //SHOW HIDE TITLE
        $ShowHideTitle = OptionsetField::create(
            $name = 'ShowHideTitle',
            $title = 'Show or Hide Title',
            $source = array(
                'show' => 'Show Section Title',
                'hide' => 'Hide Section Title',
            ),
            $value = 'hide'
        );
        //
        $SectionImageMinHeight = TextField::create('SectionImageMinHeight', 'Minimum Height')
            ->setAttribute('placeholder', 'Pixels');
        //SECTION LAYOUT
        $Layout = DropdownField::create(
            $name = 'SectionLayout',
            $title = 'Number of Content Areas',
            $source = array(
                'one' => 'One area',
                'two' => 'Two areas',
                'three' => 'Three areas',
                'four' => 'Four areas',
                'five' => 'Five areas',
            ),
            $value = 'one'
        );
        //TWO COLUMN LAYOUT
        $TwoColumnLayout = OptionsetField::create(
            $name = 'TwoColumnLayout',
            $title = 'Layout',
            $source = array(
                '2575' => '<img src="content-layout-module/images/layout-icons/25-75.png" alt="25% - 75%" title="25% - 75%" />',
                '3070' => '<img src="content-layout-module/images/layout-icons/30-70.png" alt="30% - 70%" title="30% - 70%" />',
                '4060' => '<img src="content-layout-module/images/layout-icons/40-60.png" alt="40% - 60%" title="40% - 60%" />',
                '5050' => '<img src="content-layout-module/images/layout-icons/50-50.png" alt="50% - 50%" title="50% - 50%" />',
                '6040' => '<img src="content-layout-module/images/layout-icons/60-40.png" alt="60% - 40%" title="60% - 40%" />',
                '6634' => '<img src="content-layout-module/images/layout-icons/60-40.png" alt="66% - 34%" title="66% - 34%" />',
                '7030' => '<img src="content-layout-module/images/layout-icons/70-30.png" alt="70% - 30%" title="70% - 30%" />',
                '7525' => '<img src="content-layout-module/images/layout-icons/75-25.png" alt="75% - 25%" title="75% - 25%" />',
            ),
            $value = '5050'
        );
        //THREE COLUMN LAYOUT
        $ThreeColumnLayout = OptionsetField::create(
            $name = 'ThreeColumnLayout',
            $title = 'Layout',
            $source = array(
                '333333' => '<img src="content-layout-module/images/layout-icons/33-33-33.png" alt="33% - 33% - 33%" title="33% - 33% - 33%" />',
                '252550' => '<img src="content-layout-module/images/layout-icons/25-25-50.png" alt="25% - 25% - 50%" title="25% - 25% - 50%" />',
                '1004060' => '<img src="content-layout-module/images/layout-icons/100-40-60.png" alt="100% - 40% - 60%" title="100% - 40% - 60%" />',
                '505050' => '<img src="content-layout-module/images/layout-icons/50-50-50.png" alt="50% - 50% - 50%" title="50% - 50% - 50%" />',
                '604040' => '<img src="content-layout-module/images/layout-icons/60-40-40.png" alt="60% - 40% - 40%" title="60% - 40% - 40%" />',
                '703030' => '<img src="content-layout-module/images/layout-icons/70-30-30.png" alt="70% - 30% - 30%" title="70% - 30% - 30%" />',
                '505050-flipped' => '<img src="content-layout-module/images/layout-icons/50-50-50-flipped.png" alt="50% - 50% - 50%" title="50% - 50% - 50%" />',
            ),
            $value = '333333'
        );
        //FOUR COLUMN LAYOUT
        $FourColumnLayout = OptionsetField::create(
            $name = 'FourColumnLayout',
            $title = 'Layout',
            $source = array(
                '25252525' => '<img src="content-layout-module/images/layout-icons/25-25-25-25.png" alt="25% - 25% - 25% - 25%" title="25% - 25% - 25% - 25%" />',
                '1005050100' => '<img src="content-layout-module/images/layout-icons/100-50-50-100.png" alt="100% - 50% - 50% - 100%" title="100% - 50% - 50% - 100%" />',
                '25755050' => '<img src="content-layout-module/images/layout-icons/25-75-50-50.png" alt="25% - 75% - 50% - 50%" title="25% - 75% - 50% - 50%" />',
                '50505050' => '<img src="content-layout-module/images/layout-icons/50-50-50-50.png" alt="50% - 50% - 50% - 50%" title="50% - 50% - 50% - 50%" />',
            ),
            $value = '25252525'
        );
        //SECTION BACKGROUND IMAGE
        $SectionBackgroundImage = UploadField::create('SectionBackgroundImage', 'Section Background Image')
            ->setFolderName('SectionBackgroundImages');
        $SectionBackgroundImage->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png'));

        //SECTION BACKGROUND CLASS
        $SectionBackgroundClass = TextField::create('SectionBackgroundClass', 'Section Class');

        //SECTION BACKGROUND COLOR
        $SectionBackgroundColor = TextField::create('SectionBackgroundColor', 'Section Background Color')
            ->setAttribute('placeholder', '#FFFFFF');

        //SECTION ANCHOR TAG
        $SectionAnchorTag = TextField::create('SectionAnchorTag', 'Section Anchor Tag');

        //CONTENT TYPES
        $content_types = array(
            'empty' => 'Empty',
            'html' => 'HTML Content',
            'video' => 'Video',
            'image' => 'Image',
            'iframe' => 'IFrame',
            'gallery' => 'Gallery',
            'form' => 'Form',
        );

        //SEE IF THE BLOG MODULE IS INSTALLED
        if (class_exists('Article')) {
            $content_types['blog'] = 'Blog Listing';
        }
        //SEE IF THE CALENDAR MODULE IS INSTALLED
        if (class_exists('Event')) {
            $content_types['calendar'] = 'Event Listing';
        }
        //
        if (class_exists('Testimonial')) {
            $content_types['testimonial'] = 'Testimonial';
        }
        //CONTENT 1 TYPE
        $Content1Type = DropdownField::create(
            'Content1Type',
            'Content 1 Type',
            $content_types,
            'empty'
        );
        $Content2Type = DropdownField::create(
            'Content2Type',
            'Content 2 Type',
            $content_types,
            'empty'
        );
        $Content3Type = DropdownField::create(
            'Content3Type',
            $title = 'Content 3 Type',
            $content_types,
            'empty'
        );
        $Content4Type = DropdownField::create(
            'Content4Type',
            'Content 4 Type',
            $content_types,
            'empty'
        );
        $Content5Type = DropdownField::create(
            'Content5Type',
            'Content 5 Type',
            $content_types,
            'empty'
        );

        //TWO COLUMN IMAGE LAYOUT
        $TwoColumnImageLayout = OptionsetField::create(
            'TwoColumnImageLayout',
            'Alignment',
            array(
              'left' => 'Left',
              'right' => 'Right',
            ),
           'left'
        )
            ->setDescription('Set whether you want the Content 1 Type to appear on the left or right of Content 2 Type.');

        //TWO COLUMN HANGE IMAGE LAYOUT
        $TwoColumnHangImageLayout = OptionsetField::create(
            'TwoColumnHangImageLayout',
            'Hange Image?',
            array(
                'yes' => 'Yes',
                'no' => 'No',
            ),
            'no'
        );

        $TwoColumnHangImageLayoutMaxWidth = NumericField::create('TwoColumnHangImageLayoutMaxWidth', 'Max width?');

        //CONTAIN CONTENT
        $ContainContent = OptionsetField::create(
            'ContainContent',
            'Contain Content?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'true'
        );
        //COLLAPSIBLE CONTENT
        $Collapsible = OptionsetField::create(
            'Collapsible',
            'Collapsible?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        )
            ->setDescription('This will let you hide extra content that the user can then choose to view more or not.');
        //COLLAPSIBLE CONTENT 2
        $Collapsible2 = OptionsetField::create(
            'Collapsible2',
            'Collapsible?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        )
            ->setDescription('This will let you hide extra content that the user can then choose to view more or not.');
        //COLLAPSIBLE CONTENT 3
        $Collapsible3 = OptionsetField::create(
            'Collapsible3',
            'Collapsible?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        )
            ->setDescription('This will let you hide extra content that the user can then choose to view more or not.');
        //COLLAPSIBLE CONTENT 4
        $Collapsible4 = OptionsetField::create(
            'Collapsible4',
            'Collapsible?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        )
            ->setDescription('This will let you hide extra content that the user can then choose to view more or not.');
        //COLLAPSIBLE CONTENT 5
        $Collapsible5 = OptionsetField::create(
            'Collapsible5',
            'Collapsible?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        )
            ->setDescription('This will let you hide extra content that the user can then choose to view more or not.');

        //VIDEO 1
        $Video1 = TextField::create('Video1', 'Video ID')
            ->setAttribute('placeholder', 'Please enter the id of the video');
        $Video1Type = OptionsetField::create(
            'Video1Type',
            'Where is the Video Hosted?',
            array(
                'none' => 'No Video',
                'youtube' => 'Youtube',
                'vimeo' => 'Vimeo',
            ),
            'none'
        );
        $Video1Image = UploadField::create('Video1Image', 'Video Poster Image')
            ->setFolderName('PosterImages');
        //VIDEO 2
        $Video2 = TextField::create('Video2', 'Video ID')
            ->setAttribute('placeholder', 'Please enter the id of the video');
        $Video2Type = OptionsetField::create(
            'Video2Type',
            'Where is the Video Hosted?',
            array(
                'none' => 'No Video',
                'youtube' => 'Youtube',
                'vimeo' => 'Vimeo',
            ),
            'none'
        );
        $Video2Image = UploadField::create('Video2Image', 'Video Poster Image')
            ->setFolderName('PosterImages');
        //VIDEO 3
        $Video3 = TextField::create('Video3', 'Video ID')
            ->setAttribute('placeholder', 'Please enter the id of the video');
        $Video3Type = OptionsetField::create(
            'Video3Type',
            'Where is the Video Hosted?',
            array(
                'none' => 'No Video',
                'youtube' => 'Youtube',
                'vimeo' => 'Vimeo',
            ),
            'none'
        );
        $Video3Image = UploadField::create('Video3Image', 'Video Poster Image')
            ->setFolderName('PosterImages');
        //VIDEO 4
        $Video4 = TextField::create('Video4', 'Video ID')
            ->setAttribute('placeholder', 'Please enter the id of the video');
        $Video4Type = OptionsetField::create(
            'Video4Type',
            'Where is the Video Hosted?',
            array(
                'none' => 'No Video',
                'youtube' => 'Youtube',
                'vimeo' => 'Vimeo',
            ),
            'none'
        );
        $Video4Image = UploadField::create('Video4Image', 'Video Poster Image')
            ->setFolderName('PosterImages');
        //VIDEO 5
        $Video5 = TextField::create('Video5', 'Video ID')
            ->setAttribute('placeholder', 'Please enter the id of the video');
        $Video5Type = OptionsetField::create(
            'Video5Type',
            'Where is the Video Hosted?',
            array(
                'none' => 'No Video',
                'youtube' => 'Youtube',
                'vimeo' => 'Vimeo',
            ),
            'none'
        );
        $Video5Image = UploadField::create('Video5Image', 'Video Poster Image')
            ->setFolderName('PosterImages');

        //SECTION IMAGE 1
        $SectionImage1 = UploadField::create('SectionImage1', 'Image')
            ->setFolderName('SectionImages');
        $SectionImage1->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
        $ImageCaption1 = TextAreaField::Create('ImageCaption1', 'Caption')->addExtraClass('stacked');
        $SectionImageLink1 = TextField::create('SectionImageLink1', 'Link');
        $SectionImageLinkNewWindow1 = OptionsetField::create(
            'SectionImageLinkNewWindow1',
            'New Window?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        );
        //SECTION IMAGE 2
        $SectionImage2 = UploadField::create('SectionImage2', 'Image')
            ->setFolderName('SectionImages');
        $SectionImage2->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
        $ImageCaption2 = TextAreaField::Create('ImageCaption2', 'Caption')->addExtraClass('stacked');
        $SectionImageLink2 = TextField::create('SectionImageLink2', 'Link');
        $SectionImageLinkNewWindow2 = OptionsetField::create(
            'SectionImageLinkNewWindow2',
            'New Window?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        );
        //SECTION IMAGE 3
        $SectionImage3 = UploadField::create('SectionImage3', 'Image')
            ->setFolderName('SectionImages');
        $SectionImage3->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
        $ImageCaption3 = TextAreaField::Create('ImageCaption3', 'Caption')->addExtraClass('stacked');
        $SectionImageLink3 = TextField::create('SectionImageLink3', 'Link');
        $SectionImageLinkNewWindow3 = OptionsetField::create(
            'SectionImageLinkNewWindow3',
            'New Window?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        );
        //SECTION IMAGE 4
        $SectionImage4 = UploadField::create('SectionImage4', 'Image')
            ->setFolderName('SectionImages');
        $SectionImage4->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
        $ImageCaption4 = TextAreaField::Create('ImageCaption4', 'Caption')->addExtraClass('stacked');
        $SectionImageLink4 = TextField::create('SectionImageLink4', 'Link');
        $SectionImageLinkNewWindow4 = OptionsetField::create(
            'SectionImageLinkNewWindow4',
            'New Window?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        );
        //SECTION IMAGE 5
        $SectionImage5 = UploadField::create('SectionImage5', 'Image')
            ->setFolderName('SectionImages');
        $SectionImage5->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
        $ImageCaption5 = TextAreaField::Create('ImageCaption5', 'Caption')->addExtraClass('stacked');
        $SectionImageLink5 = TextField::create('SectionImageLink5', 'Link');
        $SectionImageLinkNewWindow5 = OptionsetField::create(
            'SectionImageLinkNewWindow5',
            'New Window?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'false'
        );

        //IFRAMES
        $IFrame1 = TextAreaField::create('IFrame1', 'Enter Iframe Code');
        $IFrame2 = TextAreaField::create('IFrame2', 'Enter Iframe Code');
        $IFrame3 = TextAreaField::create('IFrame3', 'Enter Iframe Code');
        $IFrame4 = TextAreaField::create('IFrame4', 'Enter Iframe Code');
        $IFrame5 = TextAreaField::create('IFrame5', 'Enter Iframe Code');

        //GALLERIES
        $config1 = GridFieldConfig_RecordEditor::create();
        $config1->addComponent(new GridFieldBulkUpload());
        $config1->addComponent(new GridFieldBulkManager());
        $config1->addComponent(new GridFieldSortableRows('SortOrder'));
        $Gallery1 = GridField::create(
            'CLSGalleryImage1', // Field name
            'Gallery Images', // Field title
            $this->CLSGalleryImages1(), // List of all
            $config1
        );
        $config2 = GridFieldConfig_RecordEditor::create();
        $config2->addComponent(new GridFieldBulkUpload());
        $config2->addComponent(new GridFieldBulkManager());
        $config2->addComponent(new GridFieldSortableRows('SortOrder'));
        $Gallery2 = GridField::create(
            'CLSGalleryImage2', // Field name
            'Gallery Images', // Field title
            $this->CLSGalleryImages2(), // List of all
            $config2
        );
        $config3 = GridFieldConfig_RecordEditor::create();
        $config3->addComponent(new GridFieldBulkUpload());
        $config3->addComponent(new GridFieldBulkManager());
        $config3->addComponent(new GridFieldSortableRows('SortOrder'));
        $Gallery3 = GridField::create(
            'CLSGalleryImage3', // Field name
            'Gallery Images', // Field title
            $this->CLSGalleryImages3(), // List of all
            $config3
        );
        $config4 = GridFieldConfig_RecordEditor::create();
        $config4->addComponent(new GridFieldBulkUpload());
        $config4->addComponent(new GridFieldBulkManager());
        $config4->addComponent(new GridFieldSortableRows('SortOrder'));
        $Gallery4 = GridField::create(
            'CLSGalleryImage4', // Field name
            'Gallery Images', // Field title
            $this->CLSGalleryImages4(), // List of all
            $config4
        );
        $config5 = GridFieldConfig_RecordEditor::create();
        $config5->addComponent(new GridFieldBulkUpload());
        $config5->addComponent(new GridFieldBulkManager());
        $config5->addComponent(new GridFieldSortableRows('SortOrder'));
        $Gallery5 = GridField::create(
            'CLSGalleryImage5', // Field name
            'Gallery Images', // Field title
            $this->CLSGalleryImages5(), // List of all
            $config5
        );
        $GalleryTitle1 = TextField::create('GalleryTitle1', 'Gallery Title');
        $GalleryTitle2 = TextField::create('GalleryTitle2', 'Gallery Title');
        $GalleryTitle3 = TextField::create('GalleryTitle3', 'Gallery Title');
        $GalleryTitle4 = TextField::create('GalleryTitle4', 'Gallery Title');
        $GalleryTitle5 = TextField::create('GalleryTitle5', 'Gallery Title');
        //GALLERY TYPE
        $GalleryType1 = OptionsetField::create(
            $name = 'GalleryType1',
            $title = 'Type',
            $source = array(
                'Rotate' => 'Simple image rotate',
                'Slider' => 'Thumbnails, click to enlarge',
            ),
            $value = 'Rotate'
        );
        $GalleryType2 = OptionsetField::create(
            $name = 'GalleryType2',
            $title = 'Type',
            $source = array(
                'Rotate' => 'Simple image rotate',
                'Slider' => 'Thumbnails, click to enlarge',
            ),
            $value = 'Rotate'
        );
        $GalleryType3 = OptionsetField::create(
            $name = 'GalleryType3',
            $title = 'Type',
            $source = array(
                'Rotate' => 'Simple image rotate',
                'Slider' => 'Thumbnails, click to enlarge',
            ),
            $value = 'Rotate'
        );
        $GalleryType4 = OptionsetField::create(
            $name = 'GalleryType4',
            $title = 'Type',
            $source = array(
                'Rotate' => 'Simple image rotate',
                'Slider' => 'Thumbnails, click to enlarge',
            ),
            $value = 'Rotate'
        );
        $GalleryType5 = OptionsetField::create(
            $name = 'GalleryType5',
            $title = 'Type',
            $source = array(
                'Rotate' => 'Simple image rotate',
                'Slider' => 'Thumbnails, click to enlarge',
            ),
            $value = 'Rotate'
        );
        $GalleryThumbnailSize1 = TextField::create('GalleryThumbnailSize1', 'Thumbnail Size');
        $GalleryThumbnailSize2 = TextField::create('GalleryThumbnailSize2', 'Thumbnail Size');
        $GalleryThumbnailSize3 = TextField::create('GalleryThumbnailSize3', 'Thumbnail Size');
        $GalleryThumbnailSize4 = TextField::create('GalleryThumbnailSize4', 'Thumbnail Size');
        $GalleryThumbnailSize5 = TextField::create('GalleryThumbnailSize5', 'Thumbnail Size');

        //BLOG OPTIONS
        $BlogNumberOfArticles1 = NumericField::create('BlogNumberOfArticles1', 'Number of articles to show?');
        $BlogNumberOfArticles2 = NumericField::create('BlogNumberOfArticles2', 'Number of articles to show?');
        $BlogNumberOfArticles3 = NumericField::create('BlogNumberOfArticles3', 'Number of articles to show?');
        $BlogNumberOfArticles4 = NumericField::create('BlogNumberOfArticles4', 'Number of articles to show?');
        $BlogNumberOfArticles5 = NumericField::create('BlogNumberOfArticles5', 'Number of articles to show?');

        //CALENDAR OPTIONS
        $CalendarNumberOfEvents1 = NumericField::create('CalendarNumberOfEvents1', 'Events to show?');
        $CalendarNumberOfEvents2 = NumericField::create('CalendarNumberOfEvents2', 'Events to show?');
        $CalendarNumberOfEvents3 = NumericField::create('CalendarNumberOfEvents3', 'Events to show?');
        $CalendarNumberOfEvents4 = NumericField::create('CalendarNumberOfEvents4', 'Events to show?');
        $CalendarNumberOfEvents5 = NumericField::create('CalendarNumberOfEvents5', 'Events to show?');

        //FORMS
        $UserDefinedForm1 = DropDownField::create('UserDefinedForm1ID', 'Select a Form', UserDefinedForm::get()->map('ID', 'Title'))->setEmptyString('(Select one)');
        $UserDefinedForm2 = DropDownField::create('UserDefinedForm2ID', 'Select a Form', UserDefinedForm::get()->map('ID', 'Title'))->setEmptyString('(Select one)');
        $UserDefinedForm3 = DropDownField::create('UserDefinedForm3ID', 'Select a Form', UserDefinedForm::get()->map('ID', 'Title'))->setEmptyString('(Select one)');
        $UserDefinedForm4 = DropDownField::create('UserDefinedForm4ID', 'Select a Form', UserDefinedForm::get()->map('ID', 'Title'))->setEmptyString('(Select one)');
        $UserDefinedForm5 = DropDownField::create('UserDefinedForm5ID', 'Select a Form', UserDefinedForm::get()->map('ID', 'Title'))->setEmptyString('(Select one)');

        //RESPONSIVE CONTENT
        $ResponsiveContent1 = OptionsetField::create(
            'ResponsiveContent1',
            'Responsive Content?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'true'
        );
        $ResponsiveContent2 = OptionsetField::create(
            'ResponsiveContent2',
            'Responsive Content?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'true'
        );
        $ResponsiveContent3 = OptionsetField::create(
            'ResponsiveContent3',
            'Responsive Content?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'true'
        );
        $ResponsiveContent4 = OptionsetField::create(
            'ResponsiveContent4',
            'Responsive Content?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'true'
        );
        $ResponsiveContent5 = OptionsetField::create(
            'ResponsiveContent5',
            'Responsive Content?',
            array(
                '1' => 'Yes',
                '0' => 'No',
            ),
            'true'
        );

        /* HEADER/DESCRIPTION FIELDS */
        $Header_Empty = 'Empty';
        $Descri_Empty = '<p>No content will appear in this area.</p>';
        $Header_HTML = 'HTML Content';
        $Descri_HTML = '';
        $Header_Video = 'Video';
        $Descri_Video = '<p>Videos from youtube and vimeo are supported.</p>';
        $Header_Image = 'Image';
        $Descri_Image = '<p>JPG, GIF, PNG are supported.</p>';
        $Header_IFrame = 'IFrame';
        $Descri_IFrame = '';
        $Header_Gallery = 'Gallery';
        $Descri_Gallery = '';
        $Header_Blog = 'Blog';
        $Descri_Blog = '<p>This will display a summary of the latest article(s).</p>';
        $Header_Calendar = 'Calendar';
        $Descri_Calendar = '<p>This will display a summary of the latest events(s).</p>';
        $Header_Form = 'Form';
        $Descri_Form = '<p>First create a "User Defined Form".</p>';

        return new FieldList(
            new TabSet(
                'Root',
                new Tab(
                    'Settings',
                    $Title,
                    $ClmType,
                    $ShowHideTitle,
                    $TitleHTag->displayIf('ShowHideTitle')->isEqualTo('show')->end(),
                    ToggleCompositeField::create('BackgroundToggle', 'Background',
                        array(
                            $SectionBackgroundColor,
                            $SectionImageMinHeight,
                            $SectionBackgroundImage,
                        )
                    ),
                    ToggleCompositeField::create('AdvancedSettingsToggle', 'Advanced Settings',
                        array(
                            ReadOnlyField::create('SortOrder', 'SortOrder'),
                            TextField::create('SectionID', 'Section ID'),
                            $SectionBackgroundClass,
                            $SectionAnchorTag,
                        )
                    )
                ),
                new Tab(
                    'Layout',
                    $Layout,

                    DisplayLogicWrapper::create(
                        LiteralField::create('', "<div class='optionset_wrapper field'>"),
                        $TwoColumnLayout,
                        LiteralField::create('', '</div>')
                    )
                        ->displayIf('SectionLayout')
                        ->isEqualTo('two')
                        ->end(),

                    DisplayLogicWrapper::create(
                        LiteralField::create('', "<div class='optionset_wrapper field'>"),
                        $ThreeColumnLayout,
                        LiteralField::create('', '</div>')
                    )
                        ->displayIf('SectionLayout')
                        ->isEqualTo('three')
                        ->end(),

                    DisplayLogicWrapper::create(
                        LiteralField::create('', "<div class='optionset_wrapper field'>"),
                            $FourColumnLayout,
                        LiteralField::create('', '</div>')
                    )
                        ->displayIf('SectionLayout')
                        ->isEqualTo('four')
                        ->end(),

                    $Content1Type->displayIf('SectionLayout')->isEqualTo('one')->orIf('SectionLayout')->isEqualTo('two')->orIf('SectionLayout')->isEqualTo('three')->orIf('SectionLayout')->isEqualTo('four')->orIf('SectionLayout')->isEqualTo('five')->end(),
                    $Content2Type->displayIf('SectionLayout')->isEqualTo('two')->orIf('SectionLayout')->isEqualTo('three')->orIf('SectionLayout')->isEqualTo('four')->orIf('SectionLayout')->isEqualTo('five')->end(),
                    $Content3Type->displayIf('SectionLayout')->isEqualTo('three')->orIf('SectionLayout')->isEqualTo('four')->orIf('SectionLayout')->isEqualTo('five')->end(),
                    $Content4Type->displayIf('SectionLayout')->isEqualTo('four')->orIf('SectionLayout')->isEqualTo('five')->end(),
                    $Content5Type->displayIf('SectionLayout')->isEqualTo('five')->end(),

                    DisplayLogicWrapper::create(
                        $TwoColumnImageLayout
                    )
                        ->displayIf('SectionLayout')->isEqualTo('two')->end(),

                    $TwoColumnHangImageLayout
                        ->displayIf('SectionLayout')
                        ->isEqualTo('two')
                            ->andIf()
                                ->group()
                                    ->orIf('Content1Type')->isEqualTo('image')
                                    ->orIf('Content2Type')->isEqualTo('image')
                                ->end(),
                    /*$TwoColumnHangImageLayoutMaxWidth
                        ->displayIf("SectionLayout")
                        ->isEqualTo("two")
                            ->andIf()
                                ->group()
                                    ->orIf("Content1Type")->isEqualTo("image")
                                    ->orIf("Content2Type")->isEqualTo("image")
                                ->end(),*/
                    /*$Collapsible->displayIf("SectionLayout")->isEqualTo("one")->end(),*/

                    DisplayLogicWrapper::create(
                        ToggleCompositeField::create('AdvancedLayoutSettingsToggle', 'Advanced Settings',
                            array(
                                $ContainContent,
                            )
                        )
                    )
                ),
                new Tab(
                    'Content',

                    new TabSet(
                        'ContentAreas',
                        new Tab(
                            'Area 1',
                            //EMPTY CONTENT
                            DisplayLogicWrapper::create(
                              HeaderField::create('2', $Header_Empty),
                              LiteralField::create('', $Descri_Empty)
                            )
                                ->displayIf('Content1Type')->isEqualTo('empty')
                                ->end(),

                            //HTML CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_HTML),
                                LiteralField::create('', $Descri_HTML),
                                $Collapsible
                            )
                                ->displayIf('Content1Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be shown</strong>'),
                                HTMLEditorField::create('ContentNotCollapsible1', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Collapsible')->isEqualTo('1')
                                ->andIf('Content1Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be hidden</strong>')
                            )
                                ->displayIf('Collapsible')->isEqualTo('1')
                                ->andIf('Content1Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                HTMLEditorField::create('Content1', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Content1Type')->isEqualTo('html')
                                ->end(),

                            //VIDEO CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Video),
                                LiteralField::create('', $Descri_Video),
                                $Video1Type,
                                $Video1,
                                $Video1Image
                            )
                                ->displayIf('Content1Type')->isEqualTo('video')
                                ->end(),

                            //IMAGE CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Image),
                                LiteralField::create('', $Descri_Image),
                                $SectionImage1,
                                $this->getImagePreviewLinks(1),
                                $SectionImageLink1,
                                $SectionImageLinkNewWindow1,
                                $ImageCaption1
                            )
                                ->displayIf('Content1Type')->isEqualTo('image')
                                ->end(),

                            //IFRAME CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_IFrame),
                                LiteralField::create('', $Descri_IFrame),
                                $IFrame1
                            )
                                ->displayIf('Content1Type')->isEqualTo('iframe')
                                ->end(),

                            //RESPONSIVE
                            DisplayLogicWrapper::create(
                                $ResponsiveContent1
                            )
                                ->displayIf('Content1Type')->isEqualTo('image')
                                ->orIf('Content1Type')->isEqualTo('video')
                                ->orIf('Content1Type')->isEqualTo('iframe')
                                ->end(),

                            //GALLERY CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Gallery),
                                LiteralField::create('', $Descri_Gallery),
                                $GalleryType1,
                                $GalleryThumbnailSize1,
                                $GalleryTitle1,
                                $Gallery1
                            )
                                ->displayIf('Content1Type')->isEqualTo('gallery')
                                ->end(),

                            //BLOG CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Blog),
                                LiteralField::create('', $Descri_Blog),
                                $BlogNumberOfArticles1
                            )
                                ->displayIf('Content1Type')->isEqualTo('blog')
                                ->end(),

                            //CALENDAR CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Calendar),
                                LiteralField::create('', $Descri_Calendar),
                                $CalendarNumberOfEvents1
                            )
                                ->displayIf('Content1Type')->isEqualTo('calendar')
                                ->end(),

                            //FORM
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Form),
                                LiteralField::create('', $Descri_Form),
                                HTMLEditorField::create('FormContent1', 'This content will appear above the form')->setRows(20)->addExtraClass('stacked'),
                                $UserDefinedForm1
                            )
                                ->displayIf('Content1Type')->isEqualTo('form')
                                ->end()
                        ),

                        new Tab(
                            'Area 2',
                            //EMPTY CONTENT
                            DisplayLogicWrapper::create(
                              HeaderField::create('2', $Header_Empty),
                              LiteralField::create('', $Descri_Empty)
                            )
                                ->displayIf('Content2Type')->isEqualTo('empty')
                                ->end(),

                            //HTML CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_HTML),
                                LiteralField::create('', $Descri_HTML),
                                $Collapsible2
                            )
                                ->displayIf('Content2Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be shown</strong>'),
                                HTMLEditorField::create('ContentNotCollapsible2', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Collapsible2')->isEqualTo('1')
                                ->andIf('Content2Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be hidden</strong>')
                            )
                                ->displayIf('Collapsible2')->isEqualTo('1')
                                ->end(),
                            DisplayLogicWrapper::create(
                                HTMLEditorField::create('Content2', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Content2Type')->isEqualTo('html')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('two')
                                    ->orIf('SectionLayout')->isEqualTo('three')
                                    ->orIf('SectionLayout')->isEqualTo('four')
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //VIDEO CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Video),
                                LiteralField::create('', $Descri_Video),
                                $Video2Type,
                                $Video2,
                                $Video2Image
                            )
                                ->displayIf('Content2Type')->isEqualTo('video')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('two')
                                    ->orIf('SectionLayout')->isEqualTo('three')
                                    ->orIf('SectionLayout')->isEqualTo('four')
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //IMAGE CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Image),
                                LiteralField::create('', $Descri_Image),
                                $SectionImage2,
                                $this->getImagePreviewLinks(2),
                                $SectionImageLink2,
                                $SectionImageLinkNewWindow2,
                                $ImageCaption2
                            )
                                ->displayIf('Content2Type')->isEqualTo('image')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('two')
                                    ->orIf('SectionLayout')->isEqualTo('three')
                                    ->orIf('SectionLayout')->isEqualTo('four')
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //IFRAME
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_IFrame),
                                LiteralField::create('', $Descri_IFrame),
                                $IFrame2
                            )
                                ->displayIf('Content2Type')->isEqualTo('iframe')
                                ->end(),

                            //RESPONSIVE
                            DisplayLogicWrapper::create(
                                $ResponsiveContent2
                            )
                                ->displayIf('Content2Type')->isEqualTo('image')
                                ->orIf('Content2Type')->isEqualTo('video')
                                ->orIf('Content2Type')->isEqualTo('iframe')
                                ->end(),

                            //GALLERY CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Gallery),
                                LiteralField::create('', $Descri_Gallery),
                                $GalleryType2,
                                $GalleryThumbnailSize2,
                                $GalleryTitle2,
                                $Gallery2
                            )
                                ->displayIf('Content2Type')->isEqualTo('gallery')
                                ->end(),

                            //BLOG CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Blog),
                                LiteralField::create('', $Descri_Blog),
                                $BlogNumberOfArticles2
                            )
                                ->displayIf('Content2Type')->isEqualTo('blog')
                                ->end(),

                            //CALENDAR CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Calendar),
                                LiteralField::create('', $Descri_Calendar),
                                $CalendarNumberOfEvents2
                            )
                                ->displayIf('Content2Type')->isEqualTo('calendar')
                                ->end(),

                            //FORM
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Form),
                                LiteralField::create('', $Descri_Form),
                                HTMLEditorField::create('FormContent2', 'This content will appear above the form')->setRows(20)->addExtraClass('stacked'),
                                $UserDefinedForm2
                            )
                                ->displayIf('Content2Type')->isEqualTo('form')
                                ->end()
                        ),

                        new Tab(
                            'Area 3',
                            //EMPTY CONTENT
                            DisplayLogicWrapper::create(
                              HeaderField::create('2', $Header_Empty),
                              LiteralField::create('', $Descri_Empty)
                            )
                                ->displayIf('Content3Type')->isEqualTo('empty')
                                ->end(),

                            //HTML CONTENT
                            DisplayLogicWrapper::create(
                              HeaderField::create('2', $Header_HTML),
                              LiteralField::create('', $Descri_HTML),
                              $Collapsible3
                            )
                                ->displayIf('Content3Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be shown</strong>'),
                                HTMLEditorField::create('ContentNotCollapsible3', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Collapsible3')->isEqualTo('1')
                                ->andIf('Content3Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be hidden</strong>')
                            )
                                ->displayIf('Collapsible3')->isEqualTo('1')
                                ->end(),
                            DisplayLogicWrapper::create(
                                HTMLEditorField::create('Content3', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Content3Type')->isEqualTo('html')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('three')
                                    ->orIf('SectionLayout')->isEqualTo('four')
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //VIDEO CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Video),
                                LiteralField::create('', $Descri_Video),
                                $Video3Type,
                                $Video3,
                                $Video3Image
                            )
                                ->displayIf('Content3Type')->isEqualTo('video')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('three')
                                    ->orIf('SectionLayout')->isEqualTo('four')
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //IMAGE CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Image),
                                LiteralField::create('', $Descri_Image),
                                $SectionImage3,
                                $this->getImagePreviewLinks(3),
                                $SectionImageLink3,
                                $SectionImageLinkNewWindow3,
                                $ImageCaption3
                            )
                                ->displayIf('Content3Type')->isEqualTo('image')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('three')
                                    ->orIf('SectionLayout')->isEqualTo('four')
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //IFRAME
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_IFrame),
                                LiteralField::create('', $Descri_IFrame),
                                $IFrame3
                            )
                                ->displayIf('Content3Type')->isEqualTo('iframe')
                                ->end(),

                            //RESPONSIVE
                            DisplayLogicWrapper::create(
                                $ResponsiveContent3
                            )
                                ->displayIf('Content3Type')->isEqualTo('image')
                                ->orIf('Content3Type')->isEqualTo('video')
                                ->orIf('Content3Type')->isEqualTo('iframe')
                                ->end(),

                            //GALLERY CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Gallery),
                                LiteralField::create('', $Descri_Gallery),
                                $GalleryType3,
                                $GalleryThumbnailSize3,
                                $GalleryTitle3,
                                $Gallery3
                            )
                                ->displayIf('Content3Type')->isEqualTo('gallery')
                                ->end(),

                            //BLOG CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Blog),
                                LiteralField::create('', $Descri_Blog),
                                $BlogNumberOfArticles3
                            )
                                ->displayIf('Content3Type')->isEqualTo('blog')
                                ->end(),

                            //CALENDAR CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Calendar),
                                LiteralField::create('', $Descri_Calendar),
                                $CalendarNumberOfEvents3
                            )
                                ->displayIf('Content3Type')->isEqualTo('calendar')
                                ->end(),

                            //FORM
                            DisplayLogicWrapper::create(
                              HeaderField::create('2', $Header_Form),
                              LiteralField::create('', $Descri_Form),
                                HTMLEditorField::create('FormContent3', 'This content will appear above the form')->setRows(20)->addExtraClass('stacked'),
                                $UserDefinedForm3
                            )
                                ->displayIf('Content3Type')->isEqualTo('form')
                                ->end()
                        ),

                        new Tab(
                            'Area 4',
                            //EMPTY CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Empty),
                                LiteralField::create('', $Descri_Empty)
                            )
                                ->displayIf('Content4Type')->isEqualTo('empty')
                                ->end(),

                            //HTML CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_HTML),
                                LiteralField::create('', $Descri_HTML),
                                $Collapsible4
                            )
                                ->displayIf('Content4Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be shown</strong>'),
                                HTMLEditorField::create('ContentNotCollapsible4', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Collapsible4')->isEqualTo('1')
                                ->andIf('Content4Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be hidden</strong>')
                            )
                                ->displayIf('Collapsible4')->isEqualTo('1')
                                ->end(),
                            DisplayLogicWrapper::create(
                                HTMLEditorField::create('Content4', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Content4Type')->isEqualTo('html')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('four')
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //VIDEO CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Video),
                                LiteralField::create('', $Descri_Video),
                                $Video4Type,
                                $Video4,
                                $Video4Image
                            )
                                ->displayIf('Content4Type')->isEqualTo('video')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('four')
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //IMAGE CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Image),
                                LiteralField::create('', $Descri_Image),
                                $SectionImage4,
                                $this->getImagePreviewLinks(4),
                                $SectionImageLink4,
                                $SectionImageLinkNewWindow4,
                                $ImageCaption4
                            )
                                ->displayIf('Content4Type')->isEqualTo('image')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('four')
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //IFRAME
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_IFrame),
                                LiteralField::create('', $Descri_IFrame),
                                $IFrame4
                            )
                                ->displayIf('Content4Type')->isEqualTo('iframe')
                                ->end(),

                            //RESPONSIVE
                            DisplayLogicWrapper::create(
                                $ResponsiveContent4
                            )
                                ->displayIf('Content4Type')->isEqualTo('image')
                                ->orIf('Content4Type')->isEqualTo('video')
                                ->orIf('Content4Type')->isEqualTo('iframe')
                                ->end(),

                          //GALLERY CONTENT
                          DisplayLogicWrapper::create(
                              HeaderField::create('2', $Header_Gallery),
                              LiteralField::create('', $Descri_Gallery),
                              $GalleryType4,
                              $GalleryThumbnailSize4,
                              $GalleryTitle4,
                              $Gallery4
                          )
                              ->displayIf('Content4Type')->isEqualTo('gallery')
                              ->end(),

                            //BLOG CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Blog),
                                LiteralField::create('', $Descri_Blog),
                                $BlogNumberOfArticles4
                            )
                                ->displayIf('Content4Type')->isEqualTo('blog')
                                ->end(),

                            //CALENDAR CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Calendar),
                                LiteralField::create('', $Descri_Calendar),
                                $CalendarNumberOfEvents4
                            )
                                ->displayIf('Content4Type')->isEqualTo('calendar')
                                ->end(),

                            //FORM
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Form),
                                LiteralField::create('', $Descri_Form),
                                HTMLEditorField::create('FormContent4', 'This content will appear above the form')->setRows(20)->addExtraClass('stacked'),
                                $UserDefinedForm4
                            )
                                ->displayIf('Content4Type')->isEqualTo('form')
                                ->end()
                        ),

                        new Tab(
                            'Area 5',
                            //EMPTY CONTENT
                            DisplayLogicWrapper::create(
                              HeaderField::create('2', $Header_Empty),
                              LiteralField::create('', $Descri_Empty)
                            )
                                ->displayIf('Content5Type')->isEqualTo('empty')
                                ->end(),

                            //HTML CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_HTML),
                                LiteralField::create('', $Descri_HTML),
                                $Collapsible5
                            )
                                ->displayIf('Content5Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be shown</strong>'),
                                HTMLEditorField::create('ContentNotCollapsible5', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Collapsible5')->isEqualTo('1')
                                ->andIf('Content5Type')->isEqualTo('html')
                                ->end(),
                            DisplayLogicWrapper::create(
                                LiteralField::create('', '<strong>The content below will be hidden</strong>')
                            )
                                ->displayIf('Collapsible5')->isEqualTo('1')
                                ->end(),
                            DisplayLogicWrapper::create(
                                HTMLEditorField::create('Content5', '')->setRows(20)->addExtraClass('stacked')
                            )
                                ->displayIf('Content5Type')->isEqualTo('html')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //VIDEO CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Video),
                                LiteralField::create('', $Descri_Video),
                                $Video5Type,
                                $Video5,
                                $Video5Image
                            )
                                ->displayIf('Content5Type')->isEqualTo('video')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //IMAGE CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Image),
                                LiteralField::create('', $Descri_Image),
                                $SectionImage5,
                                $this->getImagePreviewLinks(5),
                                $SectionImageLink5,
                                $SectionImageLinkNewWindow5,
                                $ImageCaption5
                            )
                                ->displayIf('Content5Type')->isEqualTo('image')
                                ->andIf()
                                ->group()
                                    ->orIf('SectionLayout')->isEqualTo('five')
                                ->end(),

                            //IFRAME
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_IFrame),
                                LiteralField::create('', $Descri_IFrame),
                                $IFrame5
                            )
                                ->displayIf('Content5Type')->isEqualTo('iframe')
                                ->end(),

                            //RESPONSIVE
                            DisplayLogicWrapper::create(
                                $ResponsiveContent5
                            )
                                ->displayIf('Content5Type')->isEqualTo('image')
                                ->orIf('Content5Type')->isEqualTo('video')
                                ->orIf('Content5Type')->isEqualTo('iframe')
                                ->end(),

                            //GALLERY CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Gallery),
                                LiteralField::create('', $Descri_Gallery),
                                $GalleryType5,
                                $GalleryThumbnailSize5,
                                $GalleryTitle5,
                                $Gallery5
                            )
                                ->displayIf('Content5Type')->isEqualTo('gallery')
                                ->end(),

                            //BLOG CONTENT
                            DisplayLogicWrapper::create(
                              HeaderField::create('2', $Header_Blog),
                              LiteralField::create('', $Descri_Blog),
                              $BlogNumberOfArticles5
                            )
                                ->displayIf('Content5Type')->isEqualTo('blog')
                                ->end(),

                            //CALENDAR CONTENT
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Calendar),
                                LiteralField::create('', $Descri_Calendar),
                                $CalendarNumberOfEvents5
                            )
                                ->displayIf('Content5Type')->isEqualTo('calendar')
                                ->end(),

                            //FORM
                            DisplayLogicWrapper::create(
                                HeaderField::create('2', $Header_Form),
                                LiteralField::create('', $Descri_Form),
                                HTMLEditorField::create('FormContent5', 'This content will appear above the form')->setRows(20)->addExtraClass('stacked'),
                                $UserDefinedForm5
                            )
                                ->displayIf('Content5Type')->isEqualTo('form')
                                ->end()
                        )
                    )
                )
            )
        );
    }
    /**/
    public function getImagePreviewLinks($num)
    {
        if ($num == 1) {
            if ($this->SectionImage1()->exists()) {
                return LiteralField::create('', "Image Versions: <a href='".$this->SectionImage1()->getDesktopImage()->URL."' target='_blank'>Desktop</a>, <a href='".$this->SectionImage1()->getTabletImage()->URL."' target='_blank'>Tablet</a>, <a href='".$this->SectionImage1()->getPhoneImage()->URL."' target='_blank'>Phone</a>");
            }
        } elseif ($num == 2) {
            if ($this->SectionImage2()->exists()) {
                return LiteralField::create('', "Image Versions: <a href='".$this->SectionImage2()->getDesktopImage()->URL."' target='_blank'>Desktop</a>, <a href='".$this->SectionImage2()->getTabletImage()->URL."' target='_blank'>Tablet</a>, <a href='".$this->SectionImage2()->getPhoneImage()->URL."' target='_blank'>Phone</a>");
            }
        } elseif ($num == 3) {
            if ($this->SectionImage3()->exists()) {
                return LiteralField::create('', "Image Versions: <a href='".$this->SectionImage3()->getDesktopImage()->URL."' target='_blank'>Desktop</a>, <a href='".$this->SectionImage3()->getTabletImage()->URL."' target='_blank'>Tablet</a>, <a href='".$this->SectionImage3()->getPhoneImage()->URL."' target='_blank'>Phone</a>");
            }
        } elseif ($num == 4) {
            if ($this->SectionImage4()->exists()) {
                return LiteralField::create('', "Image Versions: <a href='".$this->SectionImage4()->getDesktopImage()->URL."' target='_blank'>Desktop</a>, <a href='".$this->SectionImage4()->getTabletImage()->URL."' target='_blank'>Tablet</a>, <a href='".$this->SectionImage4()->getPhoneImage()->URL."' target='_blank'>Phone</a>");
            }
        } elseif ($num == 5) {
            if ($this->SectionImage5()->exists()) {
                return LiteralField::create('', "Image Versions: <a href='".$this->SectionImage5()->getDesktopImage()->URL."' target='_blank'>Desktop</a>, <a href='".$this->SectionImage5()->getTabletImage()->URL."' target='_blank'>Tablet</a>, <a href='".$this->SectionImage5()->getPhoneImage()->URL."' target='_blank'>Phone</a>");
            }
        }
    }
    /**/
    public function Link()
    {
        return $this->Page()->Link();
    }
    /**/
    public function checkContent1()
    {
        if ($this->Content1 || $this->Video1 || $this->IFrame1 || $this->SectionImage1ID > 0 || $this->UserDefinedForm1()) {
            return true;
        } else {
            return false;
        }
    }
    /**/
    public function checkContent2()
    {
        if ($this->Content2 || $this->Video2 || $this->IFrame2 || $this->SectionImage2ID > 0 || $this->UserDefinedForm2()) {
            return true;
        } else {
            return false;
        }
    }
    /**/
    public function checkContent3()
    {
        if ($this->Content3 || $this->Video3 || $this->IFrame3 || $this->SectionImage3ID > 0 || $this->UserDefinedForm3()) {
            return true;
        } else {
            return false;
        }
    }
    /**/
    public function checkContent4()
    {
        if ($this->Content4 || $this->Video4 || $this->IFrame4 || $this->SectionImage4ID > 0 || $this->UserDefinedForm4()) {
            return true;
        } else {
            return false;
        }
    }
    /**/
    public function checkContent5()
    {
        if ($this->Content5 || $this->Video5 || $this->IFrame5 || $this->SectionImage5ID > 0 || $this->UserDefinedForm5()) {
            return true;
        } else {
            return false;
        }
    }
    /**/
    public function Published()
    {
        $retVal = null;
        if ($this->isPublished()) {
            $lastPub = $this->Versions('WasPublished=1', 'Version DESC', 1);
            $pub = $lastPub->pop();
            $pubEditedTime = new DateTime($pub->LastEdited);
            $retVal = $pubEditedTime->format('Y-m-d H:i:s');
        }

        return $retVal;
    }
    /**/
    public function Modified()
    {
        $retVal = null;
        if ($this->stagesDiffer('Stage', 'Live')) {
            $thisEditedTime = new DateTime($this->LastEdited);
            $retVal = $thisEditedTime->format('Y-m-d H:i:s');
        }

        return $retVal;
    }
    /**/
    public function canPublish($member = null)
    {
        $className = get_class($this);
        if (Permission::check("PUBLISH_$className", 'any', $member)) {
            return true;
        } else {
            return false;
        }
    }
    /**/
    public function canDeleteFromLive($member = null)
    {
        return $this->canPublish($member);
    }
    /**/
    public function isNew()
    {
        if (empty($this->ID)) {
            return true;
        }

        if (is_numeric($this->ID)) {
            return false;
        }

        return stripos($this->ID, 'new') === 0;
    }
    /**/
    public function isPublished()
    {
        if ($this->isNew()) {
            return false;
        }
        $className = get_class($this);

        return (DB::query('SELECT "ID" FROM "'.$className."_Live\" WHERE \"ID\" = {$this->ID}")->value()) ? true : false;
    }
    /**/
    public function onBeforeDelete()
    {
        $className = get_class($this);
        $id = $this->ID;

        DB::query("DELETE FROM {$className}_Live WHERE ID=$id");

        parent::onBeforeDelete();
    }
    /**/
    public function getForm($column = 1)
    {
        if ($column == 1) {
            $form = UserDefinedForm::get()->byID($this->UserDefinedForm1ID);
        } elseif ($column == 2) {
            $form = UserDefinedForm::get()->byID($this->UserDefinedForm2ID);
        } elseif ($column == 3) {
            $form = UserDefinedForm::get()->byID($this->UserDefinedForm3ID);
        } elseif ($column == 4) {
            $form = UserDefinedForm::get()->byID($this->UserDefinedForm4ID);
        } elseif ($column == 5) {
            $form = UserDefinedForm::get()->byID($this->UserDefinedForm5ID);
        }
        $doSet = new UserDefinedForm_Controller($form);

        return $doSet;
    }
    /**/
    public function getLatestArticles($limit = 3, $tag = '', $featured = false)
    {
        $filter_array = array();

        if ($featured) {
            $filter_array['Featured'] = 'yes';
        }
        if (strlen($tag) > 0) {
            $filter_array['Tags'] = $tag;
        }

        $articles = Article::get()
            ->filter($filter_array)
            ->exclude(array(
                'Date:GreaterThanOrEqual' => date('Y-m-d H:i:s'),
            ))
            ->sort('Date DESC')
            ->limit($limit);

        return $articles;
    }
    /**/
    public function getEvents($limit = 0)
    {
        $cuttoff_date = date('Y-m-d');
        $sql = "
			CASE
				WHEN
					DateType='multiple_continuous'
						THEN
							StartDate >= '$cuttoff_date' OR EndDate >= '$cuttoff_date'
				WHEN
					DateType='multiple_random'
						THEN
							StartDate >= '$cuttoff_date' OR
							StartDate2 >= '$cuttoff_date' OR
							StartDate3 >= '$cuttoff_date' OR
							StartDate4 >= '$cuttoff_date' OR
							StartDate5 >= '$cuttoff_date'
			ELSE
				StartDate >= '$cuttoff_date'
			END
			";
        if ($limit > 0) {
            $events = Event::get()->where(
                $sql
            )->sort('StartDate ASC, StartTime ASC')->limit($limit);
        } else {
            $events = Event::get()->where(
                $sql
            )->sort('StartDate ASC, StartTime ASC');
        }

        return $events;
    }
    /**/
    public function getRandomTestimonials($limit = 10)
    {
        //
        return Testimonial::get()->sort('RAND()')->limit($limit);
    }
}

/**/
class ContentLayoutSection_Validator extends RequiredFields
{
    /**/
    protected $customRequired = array('Title');
    /**/
    public function __construct()
    {
        $required = func_get_args();
        if (isset($required[0]) && is_array($required[0])) {
            $required = $required[0];
        }
        $required = array_merge($required, $this->customRequired);

        parent::__construct($required);
    }
    /**/
    public function php($data)
    {
        $valid = parent::php($data);

        if ($data['Title'] == '') {
            $this->validationError('Title', 'Please enter a title', 'required');
            $valid = false;
        }

        return $valid;
    }
}
