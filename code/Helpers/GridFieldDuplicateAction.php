<?php

class GridFieldDuplicateAction implements GridField_ColumnProvider, GridField_ActionProvider {

    public function augmentColumns($gridField, &$columns) {
        if(!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-buttons');
    }

    public function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'Actions') {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($gridField) {
        return array('Actions');
    }

    public function getColumnContent($gridField, $record, $columnName) {
        if(!$record->canEdit()) return;

        $field = GridField_FormAction::create(
            $gridField,
            'DuplicateAction'.$record->ID,
            '',
            "duplicate",
            array('RecordID' => $record->ID)
        )->setAttribute('data-icon', 'arrow-circle-135-left')
        ->setAttribute('Title', 'Duplicate');;

        return $field->Field();
    }

    public function getActions($gridField) {
        return array('duplicate');
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if($actionName == 'duplicate') {
            // perform your action here
            $duplicateLayout = DataObject::get_by_id("ContentLayout",$arguments["RecordID"]);
            $duplicateLayout->duplicate();
            $duplicateLayout->invokeWithExtensions('onAfterDuplicate', $duplicateLayout);
            // output a success message to the user
            Controller::curr()->getResponse()->setStatusCode(
                200,
                'Content Layout Duplicated'
            );
        }
    }
}
