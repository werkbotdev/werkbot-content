(function($) {
	$(document).ready(function(){

		/* COLLAPSIBLE CONTENT */
		$(".collapsible").addClass("closed");
		$(".collapsible").find(".collapsible_content").hide();
		$(".collapsible_button").on('click', function() {
			$(this).parent().find(".collapsible_content").toggle();
			if($(this).hasClass("closed")){
				$(this).removeClass("closed");
				$(this).addClass("open");
			}else{
				$(this).removeClass("open");
				$(this).addClass("closed");
			}
		});

    /* TABS */
    if(jQuery().tabs) {
      $('.content-layout-tabs').tabs({
        "TriggerWidth": 600,
        "Type": "Horizontal",
        "ResizeCheck": true
      });
    }
    /* IMAGE GALLERY - ROTATE */
		if(jQuery().rotate) {
      $('.galleryrotate').each(function(){
        $(this).rotate({
  				"buttonStyle": $(this).data("controls"),
  				"timer": $(this).data("time")
  			});
      });
  		if(jQuery().colorbox) {
        $('.galleryrotate a.colorbox').each(function() {
          $(this).colorbox({
            rel: $(this).parent().data("group"),
            maxWidth: '100%',
            maxHeight: '100%',
            speed: 700,
            overlayClose: true
          });
        });
      }
		}
		/* IMAGE GALLERY - SLIDER */
		if(jQuery().slider) {
			$('.galleryslider-thumbs .slidethis').slider({
				"slide_width" : 100,
				"slides_visible" : 3,
				"loop": true
			});
			//CLICK THUMBNAIL ACTION
			$(".galleryslider-thumbs .slide a").on("click", function(){
				//HIDE ALL THE IMAGES
				$(this).parent().parent().parent().parent().parent().parent().find(".galleryslider > a").hide();
				//SHOW CLICKED IMAGE
				$("#galleryslider-"+$(this).data("image")).toggle();
			});
			//COLORBOX
			if(jQuery().colorbox) {
				$('.galleryslider > a').each(function() {
					$(this).colorbox({
						rel: $(this).parent().data("group"),
		        maxWidth: '100%',
		        maxHeight: '100%',
		        speed: 700,
		        overlayClose: true
			    });
				});
			}
		}

	});
})(jQuery);
