(function($) {
	$.entwine('ss', function($) {
		var stickyHeaderInterval;

		$(".contentlayout-field-editor tbody").entwine({
			onmatch: function() {
				var i,
					thisLevel,
					depth = 0,
					$buttonrow = $('.contentlayout-field-editor .ss-gridfield-buttonrow').addClass('stickyButtons'),
					navHeight = $('.cms-content-header.north').height() + parseInt($('.stickyButtons').css('padding-top'), 10),
					fieldEditor = $('.contentlayout-field-editor'),
					self = this;

				this._super();

				// Loop through all rows and set necessary styles
				this.find('.ss-gridfield-item').each(function() {

          if($(this).data('class') == 'ContentLayoutGroup'){
            depth++;
            thisLevel = depth;
          }else if($(this).data('class') == 'ContentLayoutGroupEnd'){
            thisLevel = depth;
            depth--;
          }else{
            thisLevel = depth;
          }
          $(this).toggleClass('inFieldGroup', thisLevel >= 1);
          for(i = 0; i <= 5; i++) {
						$(this).toggleClass('inFieldGroup-level-'+i, thisLevel >= i);
					}

          $(this).find(".col-LevelDepth").addClass("level-depth-"+thisLevel).append(
            "<span></span>"
          );
          //$("<td class='level-depth-"+thisLevel+"'></td>").insertBefore($(this).find(".col-ClassName"));
				});

				// Make sure gridfield buttons stick to top of page when user scrolls down
				stickyHeaderInterval = setInterval(function () {
					var offsetTop = fieldEditor.offset().top;
					$buttonrow.width(self.width());
					if (offsetTop > navHeight || offsetTop === 0) {
						$buttonrow.removeClass('stickyButtons');
					} else {
						$buttonrow.addClass('stickyButtons');
					};
				}, 300);
			},
			onunmatch: function () {
				this._super();

				clearInterval(stickyHeaderInterval);
			}
		});

		// When new contents are added..
		$('.contentlayout-field-editor .ss-gridfield-buttonrow .action').entwine({
			onclick: function (e) {
				this._super(e);

				this.trigger('addnewinline');
			}
		});

		$('.contentlayout-field-editor').entwine({
			onmatch: function () {
				var self = this;

				this._super();

				// When the 'Add Content' button is clicked set a one time listener.
				// When the GridField is reloaded focus on the newly added field.
				this.on('addnewinline', function () {
					self.one('reload', function () {
						//If fieldgroup, focus on the start marker
						var $newField = self.find('.ss-gridfield-item').last(), $groupEnd;
						if ($newField.attr('data-class') === 'ContentLayoutGroupEnd') {
							$groupEnd = $newField;
							$groupEnd.prev().find('.col-Title input').focus();
							$newField = $groupEnd.add($groupEnd.prev());
							$groupEnd.css('visibility', 'hidden');
						} else {
							$newField.find('.col-Title input').focus();
						}

						// animate the row positioning (add the first class)
						if (document.createElement('div').style.animationName !== void 0) {
							$newField.addClass('newField');
						}

						// Once the animation has completed
						setTimeout(function () {
							$newField.removeClass('newField').addClass('flashBackground');
							$(".cms-content-fields").scrollTop($(".cms-content-fields")[0].scrollHeight);
							if($groupEnd) {
								$groupEnd.css('visibility', 'visible');
							}
						}, 500);
					});
				});
			},
			onummatch: function () {
				this._super();
			}
		});
	});
}(jQuery));
